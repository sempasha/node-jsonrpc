/**
 * @external EventEmitter
 * @see [EventEmitter]{@link http://nodejs.org/api/events.html#events_class_events_eventemitter}
 */

/**
 * @fileOverview Модуль JSON-RPC является абстракцией для реализации диалога связанных неким каналом связи точек
 * в стиле удалённого вызова процедур, в качестве протокола сериализации используется
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification}. При этом модуль спроектирован таким образом,
 * что не имеет реализации протокола передачи данных, таким образом для его реального применения необходимо разработать
 * его расишрение под конкретный случай передачи данных.
 * @module main
 */

/**
 * Логгер, поумолчанию используется консоль
 *
 * @alias module:main.logger
 * @type {{log: Function, debug: Function, info: Function, warn: Function, error: Function}}
 */
exports.logger = console;

/**
 * JSON парсер/сериализатор
 *
 * @alias module:main.JSON
 * @type {{stringify: Function, parse: Function}}
 */
exports.JSON = JSON;

/**
 * Генерирует уникальный ID, который может быть использован в качестве [Request#id]{@link module:main.Request#id}
 *
 * @alias module:main.createIdentifier
 * @type {Function}
 * @returns {String} уникальный идентификатор
 */
exports.createIdentifier = require('node-uuid');

/**
 * Парсер сообщений
 *
 * @ignore
 * @alias module:main.parseMessage
 * @type {Function}
 */
exports.parseMessage = require('./lib/parse-message');

/**
 * Проверяет соответствие сущьности объекту типа [Promise/A]{@link http://wiki.commonjs.org/wiki/Promises/A}
 *
 * @ignore
 * @alias module:main.isPromise
 * @type {Function}
 */
exports.isPromise = require('./lib/is-promise');

/**
 * Создаёт список параметров для вызова процедуры из массива значений (список аргументов по позициям) или
 * из хэш-мап объекта (список аргументов по именам)
 *
 * @ignore
 * @alias module:main.createArgsList
 * @type {Function}
 */
exports.createArgsList = require('./lib/procedure').createArgsList;

/**
 * Задаёт список описание переметров функции в виде списка имён параметров процедуры
 *
 * @ignore
 * @alias module:main.setArgsMap
 * @type {Function}
 */
exports.setArgsMap = require('./lib/procedure').setArgsMap;

/**
 * Класс описывающий точку являющуюся субъектом/объектом JSON-RPC сообщения,
 * т.е. выполняющую роль либо клиента, вызывающего удалённые процедуры на сервере, либо самого сервера, на котором
 * происходит выполнение вызванных удалённых процедур.
 *
 * @ignore
 * @alias module:main.Peer
 * @class
 */
exports.Peer = require('./lib/peer');

/**
 * Класс описывающий сообщение запроса по протоколу
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification#request_object}
 *
 * @ignore
 * @alias module:main.Request
 * @class
 */
exports.Request = require('./lib/request');

/**
 * Класс описывающий сообщение ответа по протоколу
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification#response_object}
 *
 * @ignore
 * @alias module:main.Response
 * @class
 */
exports.Response = require('./lib/response');

/**
 * Класс описывающий ошибки по протоколу [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification#error_object}
 *
 * @ignore
 * @alias module:main.Error
 * @class
 */
exports.Error = require('./lib/error');