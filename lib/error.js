/**
 * @fileOverview [JSON-RPC 2.0 Ошибка]{@link http://www.jsonrpc.org/specification#error_object}
 * @ignore
 */

/**
 * @ignore
 */
var inherits = require('util').inherits;

/**
 * @ignore
 */
var main = require('../');

/**
 * [Ошибка]{@link http://www.jsonrpc.org/specification#error_object} отвечающая требованиям
 * [спецификации JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification}, унаследована от стандартной ошибки
 *
 * @alias module:main.Error
 * @augments Error
 * @constructor
 * @param {Number} code числовой код ошибки, протоколом
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification#error_object} зарезервированы некоторые коды
 * @param {String} message текствовое сообщение, протоколом
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification#error_object} уже определены некоторые ошибки
 * @param {?(Boolean|Number|String|Object|Error)} [data=null] данные примитивного типа или структара данных,
 * содержащие дополнительную информацию об ошибке
 * @throws {TypeError} в случае, если переданы неверные значения для параметров
 * <code>code</code>, <code>message</code> или <code>data</code>
 *
 * @example <caption>Ошибка при декодировании сообщения JSON-RPC</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var rpc_json = '{"id": "100", "method": "doSomething", "params": "Invalid params"}';
 * try {
 *     var rpc = JSON.parse(rpc_json);
 * } catch (error) {
 *     throw new jsonrpc.Error(-32700, 'Parse error');
 * }
 *
 * @example <caption>Ошибка в формате сообщения JSON-RPC</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var rpc_json = {"id": "100", "method": "doSomething", "params": "Invalid params"};
 * if (typeof rpc.params !== 'undefined' && !(rpc.params instanceof Array) && !(rpc.params instanceof Object)) {
 *     throw new jsonrpc.Error(-32600, 'Invalid Request');
 * }
 */
module.exports = exports = function (code, message, data) {
    if (!(this instanceof exports)) {
        return new exports(code, message, data);
    }

    var type_code = typeof code,
        type_message = typeof message,
        type_data = typeof data;

    if (type_code !== 'number' || (code % 1 !== 0)) {
        throw new TypeError('Invalid code, must be an integer, but ' + type_code + ' (' + code + ') given');
    }
    if (type_message !== 'string') {
        throw new TypeError('Invalid message, must be a string, but ' + type_message + ' given');
    }
    if (type_data === 'undefined') {
        data = null;
    } else if (data !== null) {
        if (type_data === 'object') {
            if (!(data instanceof Array) && !(data instanceof Error) && data.constructor !== Object) {
                throw new TypeError('Invalid data, must be a structured data (Object, Array, Error) or primitive, ' +
                    'but ' + type_data + ' given');
            }
        } else if (type_data !== 'boolean' && type_data !== 'string' && type_data !== 'number') {
            throw new TypeError('Invalid data, must be a structured or primitive data, but ' + type_data + ' given');
        }
    }
    this.code = code;
    this.message = message;
    this.data = data;
};

inherits(exports, Error);

/**
 * Код ошибки, в спецификации JSON-RPC 2.0 описано использование кодов ошибок:<ol>
 *     <li>Коды пользовательский ошибок не должны попадать в диапазоны от <code>-32768</code> до <code>-32000</code>,
 *     этот диапазон зарезервирован за протоколом JSON-RPC, его текущими и будущими версиями;</li>
 *     <li>Уже описаны в спецификации ошибки с кодами: <code>-32700</code>, <code>-32600</code>, <code>-32601</code>,
 *     <code>-32602</code> и <code>-32603</code>;</li>
 *     <li>Коды в диапазоне от <code>-32000</code> до <code>-32099</code> зарезервированы под различные реализации
 *     JSON-RPC, соответственно могут быть использованы в данном модуле и в его расширениях;</li>
 * </ol>
 *
 * @alias module:main.Error#code
 * @type {Number}
 */
exports.prototype.code = null;

/**
 * Тесктовое сообщение об ошибке, в [спецификации]{@link http://www.jsonrpc.org/specification#error_object}
 * указаны сообщения для всех определённых в ней ошибок.
 *
 * @alias module:main.Error#message
 * @type {String}
 */
exports.prototype.message = null;

/**
 * Дополнительные данные для описания ошибки, в [спецификации]{@link http://www.jsonrpc.org/specification#error_object}
 * определено, что это может быть любое простое значение, либо структура данных (тип Object)
 *
 * @alias module:main.Error#data
 * @type {?(Boolean|Number|String|Object)}
 */
exports.prototype.data = null;

/**
 * Возвращает представление ошибки в виде
 * [простого объекта]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object}
 * в соответствии со [спецификацией]{@link http://www.jsonrpc.org/specification#error_object}
 *
 * @alias module:main.Error#toJSON
 * @returns {Object}
 */
exports.prototype.toJSON = function () {
    var plain = {code: this.code, message: this.message};
    if (this.data !== null) {
        plain.data = this.data;
    }
    return plain;
};

/**
 * Преобразует объект в строку, а именно в JSON представление
 *
 * @alias module:main.Error#toString
 * @returns {String}
 */
exports.prototype.toString = function () {
    return main.JSON.stringify(this);
};