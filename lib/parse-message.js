/**
 * @fileOverview Парсер сообщений [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification}
 * @ignore
 */

/**
 * @ignore
 */
var main = require('../');

/**
 * Разбирает JSON строку и проводит сопоставление полученного объекта с сообщениями
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification} согласно спецификации. В случае, если текстовое
 * сообщение имеет неверный формат будет выброшена ошибка
 * [<code>Parse error</code>]{@link http://www.jsonrpc.org/specification#error_object} с кодом
 * <code>-32700</code>, если же декодированный объект имеет неверный формат
 * (не хватает полей, либо поля имеют неверный тип), то будет выброшена ошибка
 * [<code>Invalid Request</code>]{@link http://www.jsonrpc.org/specification#error_object} с кодом <code>-32600</code>.
 *
 * @alias module:main.parseMessage
 * @param {string} json принимает объект сообщения
 * @returns {module:main.Request|module:main.Response} возвращает `JSON-RPC` сообщение
 * @throws {module:main.Error} <ol>
 *      <li>[Ошибка]{@link module:main.Error} с кодом <code>-32700</code> и сообщением
 *      [<code>Parse error</code>]{@link http://www.jsonrpc.org/specification#error_object} в случае, если
 *      исходная строка сообщнеия имеет неверный формат;</li>
 *      <li>[Ошибка]{@link module:main.Error} с кодом <code>-32600</code> и сообщением
 *      [<code>Invalid Request</code>]{@link http://www.jsonrpc.org/specification#error_object} в случае, если
 *      сообщнеия не соответствуют спецификации [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification},
 *      о несоответствии так же можно судить при получении ошибки от конструктора сообщения
 *      ([Request]{@link module:main.Request} и [Response]{@link module:main.Response})
 *      ошибок с типом
 *      [RangeError]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RangeError}
 *      или
 *      [TypeError]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypeError}
 *      </li>
 *      <li>[Ошибка]{@link module:main.Error} с кодом <code>-32603</code> и сообщением
 *      [<code>Internal error</code>]{@link http://www.jsonrpc.org/specification#error_object} в случае, если
 *      выполнение конструктора сообщения было прервано ошибкой отличной от
 *      [RangeError]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RangeError}
 *      или
 *      [TypeError]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypeError}
 *      </li>
 *  </ol>
 */
module.exports = exports = function (json) {
    var message, has_jsonrpc, has_id, has_method, has_params, has_result, has_error;

    try {
        message = main.JSON.parse(json);
    } catch (error) {
        throw new main.Error(-32700, 'Parse error', json);
    }

    if (typeof message !== 'object' || message === null) {
        throw new main.Error(-32600, 'Invalid Request', json);
    }

    has_jsonrpc = typeof message.jsonrpc !== 'undefined';
    has_id = typeof message.id !== 'undefined';
    has_method = typeof message.method !== 'undefined';
    has_params = typeof message.params !== 'undefined';
    has_result = typeof message.result !== 'undefined';
    has_error = typeof message.error !== 'undefined';

    if (!has_jsonrpc || message.jsonrpc !== '2.0') {
        throw new main.Error(-32600, 'Invalid Request', json);
    }

    try {
        if (has_method) {
            if (has_result || has_error) {
                throw new main.Error(-32600, 'Invalid Request', json);
            }
            if (has_params) {
                return new main.Request(has_id ? message.id : null, message.method, message.params);
            } else {
                return new main.Request(has_id ? message.id : null, message.method);
            }
        } else if (has_result) {
            if (has_error) {
                throw new main.Error(-32600, 'Invalid Request', json);
            }
            return new main.Response(has_id ? message.id : null, null, message.result);
        } else if (has_error) {
            if (has_result) {
                throw new main.Error(-32600, 'Invalid Request', json);
            }
            if (typeof message.error !== 'object'
                || message.error === null
                || typeof message.error.code !== 'number'
                || typeof message.error.message !== 'string')
            {
                throw new main.Error(-32600, 'Invalid Request', json);
            }
            return new main.Response(has_id ? message.id : null, new main.Error(
                message.error.code,
                message.error.message,
                typeof message.error.data !== 'undefined' ? message.error.data : null));
        } else {
            throw new main.Error(-32600, 'Invalid Request', json);
        }
    } catch (error) {
        if (error instanceof main.Error) {
            throw error;
        } else if ((error instanceof TypeError) || (error instanceof RangeError)) {
            throw new main.Error(-32600, 'Invalid Request', json);
        } else {
            throw new main.Error(-32603, 'Internal error', error);
        }
    }
};