/**
 * @fileOverview Создаёт список аргументов для вызова функции.
 * @ignore
 */
/* jshint ignore:start */
/**
 * На вход принимает предполагаемый список параметров и функцию, при вызове которой будут применеы данные параметры.
 * Правида, для составления списка параметров вызова функции:<ul>
 *     <li>Предполагаемый список аргументов может быть задан двумя способами:<ul>
 *         <li>По порядку (в виде массива) - i-ый элемент массива содержит значение i-ого аргумента функции;</li>
 *         <li>По имени (в виде хэш-мап объекта) - значение элемента списка под именем  соответствует аргументу функции с таким же именем</li>
 *     </ul></li>
 *     <li>Длина возвращаемого списка всегда совпадает с количеством аргументов функции.</li>
 *     <li>Если в исходном списке параметров отсутствуют значения каких-либо аргументов функции, то в возвращаемом
 *     списке значение этих параметров будет равно undefined.</li>
 * </ul>
 *
 * @alias module:main.createArgsList
 * @param {Function} func - функция, для которой необходимо построить список параметров
 * @param {(Object|Array)} params список параметров функции, перечисленных по порядку (массив) или имени (объект)
 * @returns {Array} - список аргументов для вызова функции
 *
 * @example <caption>Список в виде массива</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var params = ['John', 'Doe', 'male'];
 * var func = function register_person (first_name, last_name, gender) {
 *     this.persons.push({
 *         first_name: first_name,
 *         last_name: last_name,
 *         gender: gender
 *     });
 * };
 * var args = jsonrpc.createArgsList(func, params);
 * console.log(params);
 * // [John, Doe, male]
 * console.log(args);
 * // [John, Doe, male]
 *
 * @example <caption>Список в виде объекта</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var params = {last_name: 'Doe', first_name: 'John', gender: 'male'};
 * var func = function register_person (first_name, last_name, gender) {
 *     this.persons.push({
 *         first_name: first_name,
 *         last_name: last_name,
 *         gender: gender
 *     });
 * };
 * var args = jsonrpc.createArgsList(func, params);
 * console.log(params);
 * // {last_name: Doe, first_name: John, gender: male}
 * console.log(args);
 * // [John, Doe, male]
 *
 * @example <caption>Лишние и отсутствующие параметры</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var func = function register_person (first_name, last_name, gender, birth_date) {
 *     this.persons.push({
 *         first_name: first_name,
 *         last_name: last_name,
 *         gender: gender,
 *         birth_date: birth_date
 *     });
 * };
 *
 * // недостающие параметры заменяются на undefined
 * var params1 = {first_name: 'John', last_name: 'Doe', birth_date: '1980-05-01'};
 * var args1 = jsonrpc.createArgsList(func, params1);
 * console.log(params1 + ' -> ' + args1);
 * // {first_name: 'John', last_name: 'Doe', birth_date: '1980-05-01'} -> ['John', 'Doe', undefined, '1980-05-01']
 *
 * // лишние параметры игнорируются
 * var params2 = {first_name: 'John', last_name: 'Doe', something_else: '...'};
 * var args2 = jsonrpc.createArgsList(func, params2);
 * console.log(params2 + ' -> ' + args2);
 * // {first_name: 'John', last_name: 'Doe', gender: 'male', birth_date: '1980-05-01', something_else: '...'} -> ['John', 'Doe', 'male', '1980-05-01']
 *
 * // недостающие лишние параметры игнорируются
 * var params3 = ['John', 'Doe', 'male'];
 * var args3 = jsonrpc.createArgsList(func, params3);
 * console.log(params3 + ' -> ' + args3);
 * // ['John', 'Doe', 'male'] -> ['John', 'Doe', 'male', undefined]
 *
 * // лишние параметры игнорируются
 * var params4 = ['John', 'Doe', 'male', '1980-05-01', '...'];
 * var args4 = jsonrpc.createArgsList(func, params4);
 * console.log(params4 + ' -> ' + args4);
 * // ['John', 'Doe', 'male', '1980-05-01', '...'] -> ['John', 'Doe', 'male', '1980-05-01']
 */
/* jshint ignore:end */
exports.createArgsList = function (func, params) {
    var args_map = get_function_arguments_map(func),
        args_length = args_map.length,
        args,
        i;

    if (params instanceof Array) {
        args = params.slice(0, args_length);
        while(args.length < args_length) {
            args.push(undefined);
        }
    } else if (params instanceof Object) {
        args = [];
        for (i = 0; i < args_length; i++) {
            args.push(typeof params[args_map[i]] !== 'undefined' ? params[args_map[i]] : undefined);
        }
    } else {
        args = [];
    }

    return args;
};

/**
 * Задаёт список параметров процедуры - список имём нараметров процедуры без указания их значения и т.д.
 *
 * @alias module:main.setArgsMap
 * @param {Function} func
 * @param {String[]} arguments_map
 * @returns {Function}
 */
exports.setArgsMap = function (func, arguments_map) {
    func._arguments_map = arguments_map;
    return func;
};

/**
 * Возвращает список имён аргуменов функции
 *
 * @ignore
 * @alias module:main~get_function_arguments_map
 * @param {Function} func функция для которой необходимо подготовить список названий аргументов
 * @returns {Array} список названий аргументов заданной функции
 */
function get_function_arguments_map (func) {
    if (typeof func._arguments_map === 'undefined' || !(func._arguments_map instanceof Array)) {
        var parsed_definition = func.toString().match(/^\s*function\s*\(([ a-zA-Z0-9_,]+)\)/),
            args_map = parsed_definition && parsed_definition.length > 1 ? parsed_definition[1].split(/, */) : [];

        // cache function arguments map
        if (typeof func._arguments_map === 'undefined') {
            func._arguments_map = args_map;
        }

        return args_map;
    } else {
        return func._arguments_map;
    }
}