/**
 * @fileOverview [JSON-RPC 2.0 Запрос]{@link http://www.jsonrpc.org/specification#request_object}
 * @ignore
 */

/**
 * @ignore
 */
var inherits = require('util').inherits;

/**
 * @ignore
 */
var main = require('../');

/**
 * Функция, обработчик результата выполнения вызова процедуры, вызов процедуры может закончиться
 * [успехом]{@link module:main.Response} или [ошибкой]{@link module:main.Error}
 * (либо одно, либо другое, но не оба сразу, и результат должен быть всегда).
 * Обработчик принимает 2 параметра - <code>error</code> и <code>result</code>, в случае, если вызов процедуры
 * завершился [ошибкой]{@link module:main.Error}, то первый будет содержить эту ошибку,
 * второй же будет иметь значение <code>NULL</code>, в случае же успеха, <code>error</code> будет <code>NULL</code>, а
 * <code>result</code> содержить значение из поля [Response#result][успехом]{@link module:main.Response#result}
 *
 * @typedef {Function} module:main.Request~Callback
 * @param {?Error} [error=null]
 * @param {?*} [result=null]
 * @returns {undefined}
 *
 * @example <caption>Пример обработчика</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var rpc = new jsonrpc.Request('unique_id', 'someProcedure');
 * rpc.onFinish(function (error, result) {
 *     if (error) {
 *         console.error('Procedure ' + this.method + ' has been failed with error: ' + error);
 *     } else {
 *         console.log('Procedure ' + this.method + ' has been succeed with result: ' + result);
 *     }
 * });
 */

/**
 * [Вызов процедуры]{@link http://www.jsonrpc.org/specification#request_object}, в зависимости от значения параметра
 * [id]{@link module:main.Request#id} различают два типа вызова:<ol>
 *     <li>запрос - <code>id !== NULL</code>, такой вызов подразумевает наличие ответа с вызываемой стороны.;</li>
 *     <li>уведомление - <code>id === NULL</code>, вызов не подразумевает наличие ответа с вызываемой стороны.</li>
 * </ol>
 * Для обработаки результата вызова необходимо использовать специальные функции обработчики
 * (см. [finish]{@link module:main.Request#finish}, [onFinish]{@link module:main.Request#onFinish}).
 *
 * @alias module:main.Request
 * @constructor
 * @param {?String} id [идентификатор вызова]{@link module:main.Request#id}
 * @param {String} method [название процедуры]{@link module:main.Request#method}, которая будет вызвана
 * @param {?(Array|Object)} [params=null] [параметры вызова процедуры]{@link module:main.Request#params},
 * по порядку (тип <code>Array</code>) или по названию (тип <code>Object</code>)
 * @param {?Object} [options=null] дополнительные свойства запроса, свойства предназначены для расширений библиотеки
 * @throws {TypeError} в случае, если переданы неверные значения для параметров
 * <code>id</code>, <code>method</code> или <code>params</code>
 */
module.exports = exports = function (id, method, params, options) {
    if (!(this instanceof exports)) {
        return new exports(id, method, params);
    }

    var type_id = typeof id,
        type_method = typeof method,
        type_params = typeof params;

    if (id !== null && type_id !== 'string' && (type_id !== 'number' || (id % 1 !== 0))) {
        throw new TypeError('Invalid id, must be a string, integer or null, but ' + type_id + ' given');
    }
    if (type_method !== 'string') {
        throw new TypeError('Invalid method, must be a string or null, but ' + type_method + ' given');
    }
    if (type_params === 'undefined') {
        params = null;
    } else if (params !== null && !(params instanceof Array) &&
        (!(params instanceof Object) || params.constructor !== Object.prototype.constructor))
    {
        throw new TypeError('Invalid params, must be an array or plain object (or ommited)' +
            ', but ' + type_method + ' given');
    }
    this.id = id;
    this.method = method;
    this.params = params;
    this.options = options || null;
    this._callbacks = [];
};

inherits(exports, Object);

/**
 * Идентификатор вызова процедуры. Для запросов типа уведомление равен <code>NULL</code>, в противном случае - это
 * строка или целое число, значение которой уникально в рамках пределах сообщения клиент-сервис.
 * В качестве <code>id</code> реккомендуется использовать [UUID]{@link http://en.wikipedia.org/wiki/UUID} и подобные
 *
 * @alias module:main.Request#id
 * @type {?String}
 */
exports.prototype.id = null;

/**
 * Название процедуры, которая будет вызвана на сервере
 *
 * @alias module:main.Request#method
 * @type {String}
 */
exports.prototype.method = null;

/**
 * Параметры вызова процедуры, возможны два варианта объявления структуры параметров:<ul>
 *     <li>По порядку - в этом случае, параметры задаются в виде массива, в порядке ожидаемом сервером;</li>
 *     <li>По названию - зада.тся хэш-мап объектом (Object) имена параметров соответствуют
 *     именам параметров на сервере</li>
 * </ul>
 *
 * @alias module:main.Request#params
 * @type {?(Array|Object)}
 *
 * @example <caption>Процедура на сервере</caption>
 * function updateUser (id, name, birthDate) {
 *    var user = User.retrieveBy(id);
 *    user.set('name', name);
 *    user.set('birthDate', birthDate);
 *    user.save();
 * }
 *
 * @example <caption>Задаём параметры по порядку</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var request = new jsonrpc.Request('updateUser', ['user_id', 'New user name', '1990-10-21']);
 *
 * @example <caption>Задаём параметры по имени</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var request = new jsonrpc.Request('updateUser', {id: 'user_id', birthDate: '1990-10-21', name: 'New user name'});
 */
exports.prototype.params = null;

/**
 * Список дополнительных параметров сообщения, данные параметры не имеют отношения к процедурам (именам процедур,
 * параметрам их вызова и т.д.), и могут быть использованы в расширениях библиотеки, например, для реализации
 * транспорта сообщений
 *
 * @alias  module:main.Request#options
 * @type {?Object}
 */
exports.prototype.options = null;

/**
 * Флаг, повествующий о том, был ли запрос завершён
 *
 * @alias  module:main.Request#_finished
 * @private
 * @type {Boolean}
 */
exports.prototype._finished = false;

/**
 * [Ошибка]{@link module:main.Error} с которой был завершён данный запрос (см.
 * [Request#finish]{@link module:main.Request#finish}), в случае, если данный запрос завершён без ошибки,
 * данное поле имеет значение <code>null</code>
 *
 * @alias  module:main.Request#_error
 * @private
 * @type {?module:main.Error}
 */
exports.prototype._error = null;

/**
 * Результат с которым был завершён данный запрос (см. [Request#finish]{@link module:main.Request#finish}),
 * в случае, если данный запрос завершён с ошибкой, данное поле имеет значение <code>null</code>
 *
 * @alias  module:main.Request#_result
 * @private
 * @type {?*}
 */
exports.prototype._result = null;

/**
 * Список обработчиков результата выполнения запроса, имеет значение только до тех пор, пока запрос не был завершён, а
 * подписки на его завершение уже были зарегистрированы.
 *
 * @alias  module:main.Request#_callbacks
 * @private
 * @type {Function[]}
 */
exports.prototype._callbacks = null;

/**
 * Задаёт результат выполнения запроса. В случае возникновения ошибки пр выполнении запроса необходимо передать
 * её в параметре <code>error</code>, при этом параметр <code>result</code> должен быть пропущен (либо для него задано
 * значение <code>NULL</code>). В случае успешного выполнения запроса, в параметре <code>result</code> должен
 * содержаться результат выполнения процедуры (см. [Response#result]{@link module:main.Response#result}), а параметр
 * <code>error</code> должен быть <code>NULL</code>. Для запросов типа уведомление (запросы с пустым
 * [id]{@link module:main.Request#id}) результат выполнения всегда отсутствует.
 * После того, как будет задан результат выполнения/отправки запроса, будет произведён вызов обработчиков результата,
 * заданных с помощью метода [Request#onFinish]{@link module:main.Request#onFinish}
 *
 * @alias module:main.Request#finish
 * @param {?module:main.Error} [error=null] ошибка запроса (см. класс [Error]{@link module:main.Error}),
 * <code>NULL</code> в случае отсутствия ошибки
 * @param {?*} [result=null] результат выполнения запроса, <code>NULL</code> в случае возникновения ошибки
 * @returns {undefined}
 * @throws {Error} в случае попытки повторного вызова метода
 */
exports.prototype.finish = function (error, result) {
    if (typeof error === 'undefined' || error === null) {
        if (typeof result === 'undefined') {
            result = null;
        }
        error = null;
    } else {
        if (!(error instanceof main.Error)) {
            throw new TypeError('Invalid error, must be an instance of main.Error or null');
        }
        if (typeof result !== 'undefined' && result !== null) {
            throw new TypeError('Can\'t finish request with error and result at once');
        }
        result = null;
    }
    if (this._finished) {
        if (this._error !== null) {
            throw new Error('Request already finished with error: ' + this._error.toString());
        } else {
            throw new Error('Request already finished with result: ' + main.JSON.stringify(this._result));
        }
    }
    this._finished = true;
    this._error = error;
    this._result = result;
    while(this._callbacks.length > 0) {
        this._callbacks.shift().call(this, this._error, this._result);
    }
};

/**
 * Добавляет функцию, которая будет выполнена, после того, как станет известен результат выполнения запроса. Функция
 * <code>callback</code> должна принимать 2 параметра:<ol>
 *     <li>Ошибка выполнения запроса, в случае, если запрос выполнен без ошибок, имеет значение <code>NULL</code></li>
 *     <li>Результат выполнения запроса, в случае, если запрос выполнен с ошибкой, имеет значение <code>NULL</code></li>
 * </ol>
 * Функция <code>callback</code> будет вызвана из контекста [Request]{@link module:main.Request} объекта
 *
 * @alias module:main.Request#onFinish
 * @param {module:main.Request~Callback} callback
 * @returns {undefined}
 *
 * @example <caption>Процедура на сервере</caption>
 * var jsonrpc = require('node-jsonrpc');
 * function sayHello (word) {
 *    if (word !== 'Hello!') {
 *        return new jsonrpc.Error(1, 'Tell me "Hello!", or fuck off!');
 *    }
 *    return 'Hi!'
 * }
 *
 * @example <caption>Запрос на сервер и обработка ответа</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var callback = function (error, result) {
 *     console.log(this.params.word + ' - сказал я.');
 *     if (error) {
 *         console.error(' - ' + error + ' - накричал на меня сервис.');
 *     } else {
 *         console.log(result + ' - ответил сервис');
 *     }
 * };
 *
 * (new jsonrpc.Request('request1', 'sayHello', {word: 'Привет!'})).onFinish(callback);
 * // Вывод для request1
 *  - Привет! - сказал я.
 *  - Tell me "Hello!", or fuck off! - накричал на меня сервис.
 *
 * (new jsonrpc.Request('request2', 'sayHello', {word: 'Hello!'})).onFinish(callback);
 * // Вывод для request2
 *  - Hello! - сказал я.
 *  - Hi! - ответил сервис.
 *
 * @example <caption>Уведомдение сервера и обработка результата</caption>
 * var jsonrpc = require('node-jsonrpc');
 * var callback = function (error) {
 *     if (error) {
 *         console.log(this.params.word + ' - хотел сказать я, но не смог, я не умел говорить.');
 *     } else {
 *         console.log(this.params.word + ' - сказал я.');
 *     }
 * };
 *
 * (new jsonrpc.Request(null, 'sayHello', {word: 'Привет!'})).onFinish(callback);
 * // Вывод для уведомления в случае успешной его отправки
 *  - Привет! - сказал я.
 *
 * // Вывод для уведомления в случае ошибки отправки
 *  - Привет! - хотел сказать я, но не смог, я не умел говорить.
 */
exports.prototype.onFinish = function (callback) {
    if (!(callback instanceof Function)) {
        throw new TypeError('callback must be a function, ' + (typeof callback) + ' given');
    }
    if (this._finished) {
        callback.call(this, this._error, this._result);
    } else {
        this._callbacks.push(callback);
    }
};

/**
 * Определяет состояние запроса - завершён/незавершён (см. [Request#finish]{@link module:main.Request#finish})
 *
 * @alias module:main.Request#isFinished
 * @deprecated
 * @returns {Boolean}
 */
exports.prototype.isFinished = function () {
    return this._finished;
};

/**
 * Создает ответное сообщение исходя из результата завершения процедуры, определённого вызовом метода
 * [Request#finish]{@link module:main.Request#finish}
 *
 * @alias module:main.Request#createResponse
 * @returns {module:main.Response}
 * @throws Error в случае, если результат выполнения процедуры не известен на данный момент, т.е. метод
 * [Request#finish]{@link module:main.Request#finish} ещё не был вызван для данной процедуры
 */
exports.prototype.createResponse = function () {
    if (!this._finished) {
        throw new Error('Unable to create response message for procedure which has not been finished yet');
    }
    return new main.Response(this.id, this._error, this._result);
};

/**
 * Возвращает представление запроса в виде
 * [простого объекта]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object}
 * в соответствии со [спецификацией]{@link http://www.jsonrpc.org/specification#request_object}
 *
 * @alias module:main.Request#toJSON
 * @returns {Object}
 */
exports.prototype.toJSON = function () {
    var plain = {
        jsonrpc: '2.0',
        id: this.id,
        method: this.method,
        params: this.params
    };
    if (this.id === null) {
        delete plain.id;
    }
    if (this.params === null) {
        delete plain.params;
    }
    return plain;
};

/**
 * Преобразует объект в строку, а именно в JSON представление
 *
 * @alias module:main.Request#toString
 * @returns {String}
 */
exports.prototype.toString = function () {
    return main.JSON.stringify(this);
};