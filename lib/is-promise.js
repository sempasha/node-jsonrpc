/**
 * @fileOverview Проверяет является ли сущность объектом типа [PromiseA]{@link http://wiki.commonjs.org/wiki/Promises/A}
 * @ignore
 */

/**
 * Проверяет является ли сущность объектом типа [Promise/A]{@link http://wiki.commonjs.org/wiki/Promises/A}
 * Считается, что сущьность реализует интерфейс [Promise/A]{@link http://wiki.commonjs.org/wiki/Promises/A} в случае,
 * если она является объектом, и имеет метод [then]{@link http://wiki.commonjs.org/wiki/Promises/A#Proposal}.
 *
 * @alias module:main.isPromise
 * @param {*} something сущьность, которая будет протестирована на соответствие интерфейсу
 * [Promise/A]{@link http://wiki.commonjs.org/wiki/Promises/A}
 * @returns {Boolean} - возвращает <code>true</code> если <code>something</code> удовлетворяет интерфейсу
 * [Promise/A]{@link http://wiki.commonjs.org/wiki/Promises/A}, <code>false</code> в противном случае
 */

module.exports = exports = function (something) {
    return (something instanceof Object) && (something.then instanceof Function);
};