/**
 * @fileOverview [JSON-RPC 2.0 Ответ]{@link http://www.jsonrpc.org/specification#response_object}
 * @ignore
 */

/**
 * @ignore
 */
var inherits = require('util').inherits;

/**
 * @ignore
 */
var main = require('../');

/**
 * [Ответ]{@link http://www.jsonrpc.org/specification#response} на
 * [запрос]{@link http://www.jsonrpc.org/specification#request_object}
 *
 * @alias module:main.Response
 * @constructor
 * @param {?String} id идентификатор запроса (см. [Request#id]{@link module:main.Request#id}),
 * к которому относится данный ответ
 * @param {?module:main.Error} error [ошибка]{@link module:main.Error} выполнения запроса
 * @param {?*} [result=null] результат выполнения запроса, может иметь любой тип и значение, зависит от процедуры
 * @param {?Object} [options=null] дополнительные свойства ответа, параметр предназначен для расширений библиотеки
 * @throws {TypeError} в случае, если переданы неверные значения для параметров
 * <code>id</code>, <code>error</code> или <code>result</code>
 *
 * @example <caption>Процедура на сервере</caption>
 * var jsonrpc = require('node-jsonrpc');
 * function isInt (value) {
 *     return typeof int === 'number' && (int % 1 !== 0);
 * }
 *
 * // controller
 * try {
 *     if (request.method === 'isInt') {
 *         if (request.params instanceof Array) {
 *             if (request.params.length < 1) {
 *                 throw new jsonrpc.Error(1, 'Parameter "value" is missing');
 *             }
 *             var result = isInt(request.params[0]);
 *         } else {
 *             if (typeof request.params.value === 'undefined') {
 *                 throw new jsonrpc.Error(1, 'Parameter "value" is missing');
 *             }
 *             var result = isInt(request.params.value);
 *         }
 *     } else {
 *          throw new jsonrpc.Error(-32601, 'Method not found')
 *     }
 *     var response = new jsonrpc.Response(request.id, error);
 * } catch (error) {
 *     if (!(error instanceof jsonrpc.Error)) {
 *         error = new jsonrpc.Error(0, 'Unknown error', error.toString())
 *     }
 *     var response = new jsonrpc.Response(request.id, error);
 * }
 */
module.exports = exports = function (id, error, result, options) {
    if (!(this instanceof exports)) {
        return new exports(id, error, result);
    }

    var type_id = typeof id,
        type_error = typeof error,
        type_result = typeof result;

    if (id !== null && type_id !== 'string' && (type_id !== 'number' || (id % 1 !== 0))) {
        throw new TypeError('Invalid id, must be a string, integer or null, but ' + type_id + ' given');
    }
    if (type_error === 'undefined') {
        throw new TypeError('Invalid error, must be an main.Error or null, but no error given');
    }
    if (error === null) {
        if (id === null) {
            throw new TypeError('Response with no id must have an error');
        }
        if (type_result === 'undefined') {
            throw new TypeError('Response with no error must have a result, but no result given');
        }
    } else {
        if (!(error instanceof main.Error)) {
            throw new TypeError('Invalid error, must be an instance of main.Error or null');
        }
        if (type_result !== 'undefined' && result !== null) {
            throw new TypeError('Response must have error or result, but not both at once');
        }
        result = null;
    }
    this.id = id;
    this.error = error;
    this.result = result;
    this.options = options || null;
};

inherits(exports, Object);

/**
 * [Идентификатор запроса]{@link module:main.Request#id}, к которому относится данный ответ
 *
 * @alias module:main.Response#id
 * @see module:main.Request#id
 * @type {?String}
 */
exports.prototype.id = null;

/**
 * Ошибка выполнения/отправки запроса
 *
 * @alias module:main.Response#error
 * @type {?module:main.Error}
 */
exports.prototype.error = null;

/**
 * Результат выполнения запроса
 *
 * @alias module:main.Response#result
 * @type {?*}
 */
exports.prototype.result = null;

/**
 * Список дополнительных параметров сообщения, данные параметры не имеют отношения к процедурам (именам процедур,
 * параметрам их вызова и т.д.), и могут быть использованы в расширениях библиотеки, например, для реализации
 * транспорта сообщений
 *
 * @alias  module:main.Response#options
 * @type {?Object}
 */
exports.prototype.options = null;

/**
 * Возвращает представление ответа в виде
 * [простого объекта]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object}
 * в соответствии со [спецификацией]{@link http://www.jsonrpc.org/specification#response_object}
 *
 * @alias module:main.Response#toJSON
 * @returns {Object}
 */
exports.prototype.toJSON = function () {
    var plain = {jsonrpc: '2.0', id: this.id};
    if (this.error !== null) {
        plain.error = this.error;
    } else {
        plain.result = this.result;
    }
    return plain;
};

/**
 * Преобразует объект в строку, а именно в JSON представление
 *
 * @alias module:main.Response#toString
 * @returns {String}
 */
exports.prototype.toString = function () {
    return main.JSON.stringify(this);
};