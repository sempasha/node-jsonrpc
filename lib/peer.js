/**
 * @fileOverview Абстрактный класс описывающий точку являющуюся субъектом/объектом JSON-RPC сообщения,
 * т.е. выполняющую роль либо клиента, вызывающего удалённые процедуры на сервере, либо самого сервера, на котором
 * происходит выполнение вызванных удалённых процедур. В данном классе отсутствует реализация транспорта сообщений.
 * @ignore
 */

/**
 * Событие, которое должно быть создано при возникновении ошибки в клиенте (см.
 * [описание работы с ошибками EventEmitter]{@link http://nodejs.org/api/events.html#events_class_events_eventemitter})
 *
 * @event module:main.Peer#error
 * @type {Error}
 */

/**
 * @ignore
 */
var inherits = require('util').inherits;

/**
 * @ignore
 */
var EventEmitter = require('events').EventEmitter;

/**
 * @ignore
 */
var main = require('../');

/**
 * Список параметров конструктора по умолчанию
 *
 * @alias module:main.Peer~Options
 * @typedef
 * @type {Object}
 * @property {?Number} [request_timeout=-1] Таймаут выполнения запроса (сек.), если после отправки запроса на
 * выполнение процедуры, от удалённой стороны не поступило ответа в течение этого времени, то мы считаем,
 * что запрос провалился с ошибкой <code>"Request timeout"</code> (код <code>-32001</code>).
 */
var Options = {
    request_timeout: typeof (main.REQUEST_TIMEOUT) !== 'undefined' ? main.REQUEST_TIMEOUT : -1
};

/**
 * Абстрактный класс описывающий точку являющуюся субъектом/объектом JSON-RPC сообщения,
 * т.е. выполняющую роль либо клиента, вызывающего удалённые процедуры на сервере, либо самого сервера, на котором
 * происходит выполнение вызванных удалённых процедур. В данном классе отсутствует реализация транспорта сообщений.
 *
 * @alias module:main.Peer
 * @abstract
 * @constructor
 * @augments external:EventEmitter
 * @params {Object} options список параметров, на данный момент единственный параметры - <code>request_timeout</code>
 *
 * @example <caption>Пример конечной реализации jsonrpc.Peer с каналом связи по net.Socket</caption>
 * var SocketPeer = function (options) {
 *     var self = this, data = '';
 *     jsonrpc.Peer.call(self);
 *     self._socket = options._socket;
 *     socket.on('connect', function () {
 *         self.send();
 *     });
 *     socket.on('end', function () {
 *         for (var id in self._incomingRequests) {
 *             self._incomingRequests[id].finish(new jsonrpc.Error(-32002, "Connection closed"));
 *         }
 *         for (var id in self._outgoingRequests) {
 *             self._outgoingRequests[id].finish(new jsonrpc.Error(-32002, "Connection closed"));
 *         }
 *     });
 *     socket.on('data', function (chunk) {
 *         data += chunk;
 *         var messages = data.split('\n');
 *         data = messages.pop();
 *         messages.forEach(function (message) {
 *             self._acceptMessage(message);
 *         });
 *     });
 * };
 *
 * SocketPeer.prototype = new jsonrpc.Peer();
 *
 * SocketPeer.prototype._getRequestedProcedure = function (method) {
 *     return typeof Controller[method] === 'function' ? Controller[method] : undefined;
 * };
 *
 * SocketPeer.prototype.send = function () {
 *     while(this._sendBuffer.length > 0) {
 *         var message = this._sendBuffer.shift();
 *         this._socket.write(message.toString());
 *         if (message instanceof jsonrpc.Request && message.id === null) {
 *             message.finish();
 *         }
 *     }
 * };
 */
module.exports = exports = function (options) {
    if (!(this instanceof exports)) {
        return new exports(options);
    }
    var self = this;
    self._options = {};
    if (typeof options !== 'object' || options === null || typeof options.request_timeout === 'undefined') {
        self._options.request_timeout = Options.request_timeout;
    } else {
        self._options.request_timeout = options.request_timeout;
    }
    EventEmitter.call(self);
    self._sendBuffer = [];
    self._incomingRequests = {};
    self._outgoingRequests = {};
    self.on('error', function (error) {
        self._errorHandler(error);
    });
};

inherits(exports, EventEmitter);

/**
 * Список параметров клиента, см. параметр <code>options</code>
 * [конструктора]{@link module:main.Peer#constructor}
 *
 * @alias module:main.Peer#_options
 * @protected
 * @type {module:main.Peer~Options}
 */
exports.prototype._options = null;

/**
 * Буффер исходящих сообщений
 *
 * @alias module:main.Peer#_sendBuffer
 * @protected
 * @type {Array.<module:main.Request,module:main.Response>}
 */
exports.prototype._sendBuffer = null;

/**
 * Хэш-мап список входящих запросов, ожидающих окончания выполнения
 *
 * @alias module:main.Peer#_incomingRequests
 * @protected
 * @type {Object<string,module:main.Request>}
 */
exports.prototype._incomingRequests = null;

/**
 * Хэш-мап список исходящих запросов, ожидающих окончания выполнения
 *
 * @alias module:main.Peer#_outgoingRequests
 * @protected
 * @type {Object<string,module:main.Request>}
 */
exports.prototype._outgoingRequests = null;

/**
 * Стандартный обработчик ошибок, производит логирование ошибки
 *
 * @alias module:main.Peer#_errorHandler
 * @protected
 * @param {Error} error Ошибка
 * @returns {Undefined}
 */
exports.prototype._errorHandler = function (error) {
    main.logger.error(error);
};

/**
 * Метод, который должен быть вызван при получении сообщения от удалённой стороны.
 * Метод выполняет идентификацию типа сообщения, в зависимости от типа производится сообтествующая обработка:<ol>
 *     <li>[Request]{@link module:main.Request} - вызов метода
 *     [Peer#_acceptRequest]{@link module:main.Peer#_acceptRequest}</li>
 *     <li>[Response]{@link module:main.Response} - вызов метода
 *     [Peer#_acceptResponse]{@link module:main.Peer#_acceptResponse}</li>
 *     <li>В случае ошибки [парсера]{@link module:main.parse} будет приизведена отправка ответа,
 *     содержащего данную ошибку удалённой стороне</li>
 * </ol>
 *
 * @alias module:main.Peer#_acceptMessage
 * @protected
 * @param {String} json сообщение в формате JSON
 * @returns {Boolean}
 */
exports.prototype._acceptMessage = function (json) {
    var message;

    // parse message
    try {
        message = main.parseMessage(json);
    } catch (error) {
        this._sendBuffer.push(new main.Response(null, error));
        this.send();
        return false;
    }

    if (message instanceof main.Request) {
        return this._acceptRequest(message);
    } else {
        return this._acceptResponse(message);
    }
};

/* jshint ignore:start */
/**
 * Метод, который должен быть вызван при получении сообщения типа
 * [запрос]{@link http://www.jsonrpc.org/specification#request_object} ([Request]{@link module:main.Request}).
 * Сценарий обработки запроса:<ul>
 *     <li>Проверить, существование и доступность запрошенного [метода]{@link module:main.Request#method}
 *     (см. [Peer#_getRequestedProcedure]{@link module:main.Peer#_getRequestedProcedure}), если метод недоступен
 *     (не существует, не может быть вызван), удалённой стороне будет отправлена
 *     [ошибка]{@link http://www.jsonrpc.org/specification#error_object} <code>"Method not found"</code>
 *     (код <code>-32601</code>)</li>
 *     <li>Сформировать список аргументов, с которыми будет вызвана процедура
 *     (см. [createArgsList]{@link module:main.createArgsList})</li>
 *     <li>Вызвать процедуру. Результатом вызова могут быть:<ul>
 *         <li>Поймана ошибка - ошибка будет обработана и отправлена удалённой стороне, варианты ошибок:<ul>
 *             <li>[JSON-RPC Error]{@link module:main.Error} - не требует дополнительной интерпритации</li>
 *             <li>[TypeError]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypeError}
 *             или [RangeError]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RangeError} -
 *             интерпретируется как [ошибка параметров]{@link http://www.jsonrpc.org/specification#error_object}
 *             <code>"Invalid params"</code> (код <code>-32602</code>)</li>
 *             <li>[Error]{@link module:main.Error}  - интерпретируется, как неизвестная ошибка
 *             <code>"Unknown error"</code> (код <code>-32000</code>)</li>
 *         </ul></li>
 *         <li>Получен объект типа Promise/A - </li>
 *         <li>Во всех остальных случаях полученный результат будет отправлен удалённой стороне,
 *         запросившей выполнение процедуры</li>
 *         <li></li>
 *     </ul></li>
 * </ul>
 *
 * @alias module:main.Peer#_acceptRequest
 * @protected
 * @param {module:main.Request} request запрос
 * @returns {Boolean}
 */
/* jshint ignore:end */
exports.prototype._acceptRequest = function (request) {
    var self = this,
        context = null,
        procedure,
        result;

    // register incoming request
    if (request.id !== null) {
        self._incomingRequests[request.id] = request;
        request.onFinish(function (error, result) {
            if (typeof self._incomingRequests[request.id] !== 'undefined') {
                delete self._incomingRequests[request.id];
                self._sendBuffer.push(request.createResponse());
                self.send();
            }
        });
    }

    procedure = self._getRequestedProcedure(request.method);

    if ((procedure instanceof Object)
        && typeof procedure['function'] === 'function'
        && typeof procedure.context !== 'undefined')
    {
        context = procedure.context;
        procedure = procedure['function'];
    }

    if (!(procedure instanceof Function)) {
        request.finish(new main.Error(-32601, 'Method not found'));
        return false;
    }

    try {
        result = procedure.apply(context, main.createArgsList(procedure, request.params));
    } catch (error) {
        // parameters error
        if ((error instanceof TypeError) || (error instanceof RangeError)) {
            request.finish(new main.Error(-32602, 'Invalid params', error.message));
            // common error
        } else if (!(error instanceof main.Error)) {
            request.finish(new main.Error(-32000, 'Unknown error', error.message));
            // JSON-RPC defined error
        } else {
            request.finish(error);
        }
        return false;
    }

    if (main.isPromise(result)) {
        result.then(
            function (result) {
                try {
                    request.finish(null, result);
                } catch (error) {
                    self.emit('error', error);
                }
            },
            function (error) {
                try {
                    // parameters error
                    if ((error instanceof TypeError) || (error instanceof RangeError)) {
                        request.finish(new main.Error(-32602, 'Invalid params', error.message));
                        // common error
                    } else if (!(error instanceof main.Error)) {
                        request.finish(new main.Error(-32000, 'Unknown error', error.message));
                        // JSON-RPC defined error
                    } else {
                        request.finish(error);
                    }
                } catch (error) {
                    self.emit('error', error);
                }
            });
    } else {
        request.finish(null, result);
    }

    return true;
};

/**
 * Метод, который должен быть вызван при получении сообщения типа
 * [ответ]{@link http://www.jsonrpc.org/specification#response_object} ([Response]{@link module:main.Response}).
 * При получении ответа, содержащего ошибку, но без обозначнеия <code>id</code> запроса, то будет создано событие
 * [error]{@link module:main.Peer#event:error}. При получении ответа на запрос, который не содержится в списке
 * запросов ожидающих ответа, так же будет создано событие [error]{@link module:main.Peer#event:error}
 *
 * @alias module:main.Peer#_acceptResponse
 * @protected
 * @param {module:main.Response} response ответ
 * @returns {Boolean}
 * @fires module:main.Peer#error
 */
exports.prototype._acceptResponse = function (response) {
    if (response.id !== null) {
        if (typeof this._outgoingRequests[response.id] !== 'undefined') {
            this._outgoingRequests[response.id].finish(response.error, response.result);
        } else {
            var error = new Error('Unable to process response, ' +
                'there is no request matching id "' + response.id + '", reponse: ' + response.toString());
            this.emit('error', error);
        }
    } else {
        this.emit('error', response.error);
    }
};

/**
 * Возвращает функцию соответствующую запрошенной процедуре. Для того, чтобы задать контекст выполнения процедуры в
 * ответ необходимо вернуть объект с двум полями <code>{function: Function, context: Object}</code>
 *
 * @alias module:main.Peer#_getRequestedProcedure
 * @protected
 * @abstract
 * @param {String} method имя процедуры (см. [Request#method]{@link module:main.Request#method})
 * @returns {(Function|{function: Function, context: Object})}
 */
exports.prototype._getRequestedProcedure = function (method) {
    throw new Error('Peer does not support any method');
};

/**
 * Метод производит отправку JSON-RPC сообщений из буффера. Если сообщение -
 * это запрос типа [уведомление]{@link http://www.jsonrpc.org/specification#notification}, то после того,
 * как сообщение отправлено для запроса будет зафиксирован положительный результат выполнения
 * (см. [Request#finish]{@link module:main.Request#finish}).
 *
 * @alias module:main.Peer#send
 * @abstract
 * @returns {undefined}
 */
exports.prototype.send = function () {
    throw new Error('Peer does not support message sending');
};

/**
 * Отправляет [запрос]{@link module:main.Request} на выполнение процедуры удалённой стороне
 *
 * @alias module:main.Peer#request
 * @param {String} method название процедуры (см. [Request#method]{@link module:main.Request#method})
 * @param {?(Array|Object)} [params=null] список параметров (см. [Request#params]{@link module:main.Request#params})
 * @param {?Object} [options=null] дополнительные параметры запроса
 * @returns {module:main.Request}
 */
exports.prototype.request = function (method, params, options) {
    var self = this,
        id = main.createIdentifier(),
        request = new main.Request(id, method, params, options);

    self._outgoingRequests[id] = request;
    if (self._options.request_timeout > 0) {
        request._timer = setTimeout(function () {
            request.finish(new main.Error(-32001, 'Request timeout'));
        }, self._options.request_timeout * 1000);
    }
    request.onFinish(function () {
        var index = self._sendBuffer.indexOf(request);
        if (index > -1) {
            self._sendBuffer.splice(index, 1);
        }
        delete self._outgoingRequests[id];
        if (request._timer !== null) {
            clearTimeout(request._timer);
            request._timer = null;
        }
    });
    self._sendBuffer.push(request);
    self.send();
    return request;
};

/**
 * Отправляет [запрос]{@link module:main.Request} типа
 * [уведомление]{@link http://www.jsonrpc.org/specification#notification}
 * на выполнение процедуры удалённой стороне
 *
 * @alias module:main.Peer#notify
 * @param {String} method название процедуры (см. [Request#method]{@link module:main.Request#method})
 * @param {?(Array|Object)} params список параметров (см. [Request#params]{@link module:main.Request#params})
 * @param {?Object} [options=null] дополнительные параметры уведомления
 * @returns {module:main.Request}
 */
exports.prototype.notify = function (method, params, options) {
    var self = this,
        id = main.createIdentifier(),
        request = new main.Request(null, method, params, options);

    self._outgoingRequests[id] = request;
    if (self._options.request_timeout > 0) {
        request._timer = setTimeout(function () {
            request.finish(new main.Error(-32001, 'Request timeout'));
        }, self._options.request_timeout * 1000);
    }
    request.onFinish(function () {
        var index = self._sendBuffer.indexOf(request);
        if (index > -1) {
            self._sendBuffer.splice(index, 1);
        }
        delete self._outgoingRequests[id];
        if (request._timer !== null) {
            clearTimeout(request._timer);
            request._timer = null;
        }
    });
    self._sendBuffer.push(request);
    self.send();
    return request;
};