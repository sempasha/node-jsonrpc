/**
 * @fileOverview Тесты класса [main.Peer]{@link module:main.Peer}
 * @ignore
 */

/* global describe, it, xit */
var assert = require('assert'),
    EventEmitter = require('events').EventEmitter,
    Peer = require('../lib/peer'),
    main = require('./jsonrpc.example');

describe('main.Peer', function() {
    var in_list = function (list, object) {
            for (var i in list) {
                if (list[i] === object) {
                    return true;
                }
            }
            return false;
        },
        is_same_requests = function (r1, r2) {
            return r1.toString() === r2.toString();
        };

    describe('#constructor', function () {
        it('Унаследован от EventEmitter',
            function () {
                var peer = new Peer();
                assert.ok(peer instanceof EventEmitter);
            });

        it('Устанавливает стандартный обработчик ошибок (событие error) Peer#_errorHandler',
            function () {
                var peer = new Peer(),
                    trigger_error = new Error('Sample error'),
                    handled = false;

                peer._errorHandler = function (error) {
                    handled = true;
                    delete peer._errorHandler;
                    assert.equal(error, trigger_error, 'Ошибка отличается от исходной');
                };
                peer.emit('error', trigger_error);
                assert.ok(handled, 'Ошибка не была обработана');
            });
    });

    describe('#_sendBuffer', function () {

        it('Буфер - это очередь сообщений для отправки удалённой стороне',
            function (done) {
                var peers = main.Peer.createPair(),
                    create_queue = [],
                    send_queue = [],
                    done_count = 0,
                    count = 5,
                    peer_send = peers[0].send,
                    request,
                    on_finish = function () {
                        if (++done_count === create_queue.length) {
                            create_queue.forEach(function (request, index) {
                                assert.strictEqual(request, send_queue[index], 'Ошибка очерёдности отправки сообщений');
                            });
                            done();
                        }
                    };

                peers[0].send = function () {
                    send_queue.push(this._sendBuffer[this._sendBuffer.length - 1]);
                    peer_send.call(this);
                };
                while(count-- > 0) {
                    request = peers[0].request('ping', {payload: 'hello'});
                    request.onFinish(on_finish);
                    create_queue.push(request);
                }
            });

        it('Буфер - массов объектов',
            function () {
                var peers = main.Peer.createPair();
                assert.ok(peers[0]._sendBuffer instanceof Array, 'Буфер исходящих сообщений должен быть массивом');
            });

        it('Запрос, завершившийся с ошибкой ещё до отправки должен быть удалён из буфера',
            function (done) {
                var peers = main.Peer.createPair({request_timeout: 0.05}),
                    request;

                // async sending
                peers[0].send = function (message) {
                    setTimeout(function () {
                        peers[0].send(message);
                    }, peers[0]._options.request_timeout * 1000 + 1);
                    delete peers[0].send;
                };
                request = peers[0].request('ping', {payload: 'hello'});
                request.onFinish(function (error, result) {
                    assert.strictEqual(peers[0]._sendBuffer.indexOf(request), -1, 'Запрос не удалён из буфера');
                    done();
                });
            });
    });

    describe('#_incomingRequests', function () {
        it('Список содержит входящие запросы, пока для них не будет определён результат',
            function (done) {
                var peers = main.Peer.createPair(),
                    requests = [];

                peers[1]._acceptRequest = function (request) {
                    assert.ok(!in_list(peers[1]._incomingRequests, request), 'Запрос попал в список до обработки');
                    var result = main.Peer.prototype._acceptRequest.call(this, request);
                    request.onFinish(function () {
                        assert.ok(!in_list(peers[1]._incomingRequests, request),
                            'Запрос "' + request.method + '" не удалён из списка после обработки');
                        requests = requests.filter(function (r) {return !is_same_requests(r, request);});
                        if (requests.length === 0) {
                            delete peers[1]._acceptRequest;
                            done();
                        }
                    });
                    return result;
                };
                requests.push(peers[0].request('ping', {payload: 'hello'}));
                requests.push(peers[0].request('sleep', {ms: 10}));
            });

        it('Список не должен содержать входящие запросы уведомления (не требующие ответа от сервера)',
            function (done) {
                var peers = main.Peer.createPair(),
                    requests = [];

                peers[1]._acceptRequest = function (request) {
                    var result = main.Peer.prototype._acceptRequest.call(this, request);
                    assert.ok(!in_list(peers[1]._incomingRequests, request), 'Запрос не должент быть в списке');
                    request.onFinish(function () {
                        assert.ok(!in_list(peers[1]._incomingRequests, request), 'Запрос не должент быть в списке');
                        requests = requests.filter(function (r) {return !is_same_requests(r, request);});
                        if (requests.length === 0) {
                            delete peers[1]._acceptRequest;
                            done();
                        }
                    });
                    return result;
                };
                requests.push(peers[0].notify('ping', {payload: 'hello'}));
                requests.push(peers[0].notify('sleep', {ms: 10}));
            });

        it('Список в виде хэшмап объекта, ключ - id запроса, значени - сам запрос',
            function (done) {
                var peers = main.Peer.createPair(),
                    request = peers[0].request('sleep', {ms: 10});

                request.onFinish(done);
                peers[1]._acceptRequest = function (request) {
                    var result = main.Peer.prototype._acceptRequest.call(this, request);
                    assert.ok(peers[1]._incomingRequests instanceof Object);
                    assert.ok(request.id in peers[1]._incomingRequests);
                    delete peers[1]._acceptRequest;
                    return result;
                };
            });
    });

    describe('#_outgoingRequests', function () {
        it('Список содержит исходящие запросы, которые были/будут отправлены удалённой стороне и ' +
            'на которые пока не поулчен ответ',
            function (done) {
                var peers = main.Peer.createPair(),
                    request, notification;

                request = peers[0].request('sleep', {ms: 10});
                peers[0].send = function () {
                    delete peers[0].send;
                    setTimeout(function () {
                        peers[0].send();
                    }, 10);
                };
                notification = peers[0].request('sleep', {ms: 10});
                assert.ok(in_list(peers[0]._outgoingRequests, request), 'Запрос должен содержаться в списке до тех ' +
                    'пор, пока на него не будет получен ответ');
                assert.ok(in_list(peers[0]._outgoingRequests, notification), 'Уведомление должно содержаться в ' +
                    'списке до тех пор, пока не будет получен отправлено');
                request.onFinish(function () {
                    assert.ok(!in_list(peers[0]._outgoingRequests, request), 'Запрос должен быть удалён из списка ' +
                        'после получения ответа');
                    done();
                });
                notification.onFinish(function () {
                    assert.ok(!in_list(peers[0]._outgoingRequests, request), 'Уведомление должно быть удалено из ' +
                        'списка после отправки');
                });
            });

        it('Список не должен содержать запросы, которые были завершены до получения ответа от удалённой стороны ' +
            '(например по таймауту)',
            function (done) {
                var peers = main.Peer.createPair(),
                    request;

                request = peers[0].request('ping', {payload: 'hello'});
                request.onFinish(function (error, result) {
                    assert.ok(!in_list(peers[0]._outgoingRequests, request), 'Запрос должен быть удалён из списка ' +
                        'после отправки');
                    done();
                });
            });

        it('Список в виде хэшмап объекта, ключ - id запроса, значени - сам запрос',
            function (done) {
                var peers = main.Peer.createPair(),
                    request = peers[0].request('ping', {payload: 'hello'});

                assert.ok(peers[0]._outgoingRequests instanceof Object);
                assert.ok(request.id in peers[0]._outgoingRequests);
                request.onFinish(done);
            });
    });

    describe('#_acceptMessage', function () {
        it('Проивзодит парсинг сообщения методом main.parseMessage',
            function () {
                var peers = main.Peer.createPair(),
                    called = false;

                main.parseMessage = (function (parse) {
                    return function (message) {
                        called = true;
                        main.parseMessage = parse;
                        return parse(message);
                    };
                })(main.parseMessage);
                peers[0]._acceptMessage('{"jsonrpc":"2.0","id":null,"error":{"code":0,"message":"void"}}');
                assert.ok(called, 'main.parseMessage не была вызвана');
            });

        it('При ошибке парсинга отправляет удалённой стороне соответствующую ошибку парсера c ' +
            'указанием текста неверного запроса в поле main.Error#data',
            function (done) {
                var peers = main.Peer.createPair(),
                    invalid_messages = ['"', '{}', '{"id":null,"error":{"code":0,"message":"void"}}'],
                    response_acceped = 0;

                peers[1]._acceptResponse = function (response) {
                    assert.ok(response.error instanceof main.Error, 'Сообщение с ошибкой парсинга не отправлено ' +
                        'удалённой стороне');

                    if (response.error instanceof main.Error) {
                        assert.ok(invalid_messages.indexOf(response.error.data) > -1, 'Сообщение с ошибкой парсинга ' +
                            'не содержит исходное сообщение, вызвавшее ошибку в поле main.Error#data');
                    }

                    if (++response_acceped === invalid_messages.length) {
                        delete peers[1]._acceptResponse;
                        done();
                    }
                };
                invalid_messages.forEach(function (message) {
                    peers[0]._acceptMessage(message);
                });
            });

        it('Если получено сообщение типа запрос - будет вызван метод main.Peer#_acceptRequest',
            function () {
                var peers = main.Peer.createPair(),
                    called = false;

                peers[0]._acceptRequest = function () {
                    called = true;
                    delete peers[0]._acceptRequest;
                };
                peers[0]._acceptMessage('{"jsonrpc":"2.0","id":"id","method":"ping","params":{"payload":"hello"}}');
                assert.ok(called);
            });

        it('Если получено сообщение типа ответ - будет вызван метод main.Peer#_acceptResponse',
            function () {
                var peers = main.Peer.createPair(),
                    called = false;

                peers[0]._acceptResponse = function () {
                    called = true;
                    delete peers[0]._acceptResponse;
                };
                peers[0]._acceptMessage('{"jsonrpc":"2.0","id":"id","result":"hello"}');
                assert.ok(called);
            });
    });

    describe('#_acceptRequest', function () {
        it('Вызывает метод main.Peer#_getRequestedProcedure для определения функции соответствующей ' +
            'запрошенной процедуре',
            function () {
                var peers = main.Peer.createPair(),
                    called = false;

                peers[0]._getRequestedProcedure = function () {
                    called = true;
                    delete peers[0]._getRequestedProcedure;
                };
                peers[0]._acceptMessage('{"jsonrpc":"2.0","method":"ping"}');
                assert.ok(called);
            });

        it('При недоступности функции процедуры завершает запрос с ошибкой -32601, "Method not found"',
            function (done) {
                var peers = main.Peer.createPair();

                peers[0].request('non_existing_method', {payload: 'hello'}).onFinish(function (error) {
                    assert.ok(error instanceof main.Error, 'Ошибка не получена');
                    assert.strictEqual(error.code, -32601, 'Неверный код ошибки');
                    assert.strictEqual(error.message, 'Method not found', 'Неверное сообщение ошибки');
                    done();
                });
            });

        it('Производит формирование списка параметров для вызова процедуры вызовом метода main.createArgsList',
            function (done) {
                var peers = main.Peer.createPair(),
                    called = false;

                peers[0].request('ping', {payload: 'hello'}).onFinish(function (error) {
                    assert.ok(called, 'Метод не был вызван');
                    done();
                });
                main.createArgsList = (function (old) {
                    return function () {
                        called = true;
                        main.createArgsList = old;
                        return main.createArgsList.apply(null, arguments);
                    };
                })(main.createArgsList);
            });

        it('Производит вызов процедуры',
            function (done) {
                var peers = main.Peer.createPair(),
                    called = false;

                peers[0].request('test').onFinish(function (error) {
                    delete main.Peer.Controller.test;
                    assert.ok(called, 'Метод не был вызван');
                    done();
                });
                main.Peer.Controller.test = function () {
                    called = true;
                };
            });

        it('Если функция процедуры выдаст ошибку TypeError или RangeError завершает запрос с ' +
            'ошибкой -32602, "Invalid params"',
            function (done) {
                var peers = main.Peer.createPair(),
                    completed = 0;

                peers[0].request('testTypeError').onFinish(function (error) {
                    delete main.Peer.Controller.testTypeError;
                    assert.ok(error instanceof main.Error, 'Ошибка не получена');
                    assert.strictEqual(error.code, -32602, 'Неверный код ошибки');
                    assert.strictEqual(error.message, 'Invalid params', 'Неверное сообщение ошибки');
                    if (++completed === 2) {
                        done();
                    }
                });
                peers[0].request('testRangeError').onFinish(function (error) {
                    delete main.Peer.Controller.testRangeError;
                    assert.ok(error instanceof main.Error, 'Ошибка не получена');
                    assert.strictEqual(error.code, -32602, 'Неверный код ошибки');
                    assert.strictEqual(error.message, 'Invalid params', 'Неверное сообщение ошибки');
                    if (++completed === 2) {
                        done();
                    }
                });
                main.Peer.Controller.testTypeError = function () {
                    throw new TypeError('Invalid Type');
                };
                main.Peer.Controller.testRangeError = function () {
                    throw new RangeError('Invalid Range');
                };
            });

        it('Если функция процедуры выдаст ошибку main.Error завершает запрос с ' +
            'этой ошибкой',
            function (done) {
                var peers = main.Peer.createPair();

                peers[0].request('testJsonrpcError').onFinish(function (error) {
                    delete main.Peer.Controller.testJsonrpcError;
                    assert.ok(error instanceof main.Error, 'Ошибка не получена');
                    assert.strictEqual(error.code, 666, 'Неверный код ошибки');
                    assert.strictEqual(error.message, 'Sample Error', 'Неверное сообщение ошибки');
                    done();
                });
                main.Peer.Controller.testJsonrpcError = function () {
                    throw new main.Error(666, 'Sample Error');
                };
            });

        it('Если функция процедуры выдаст любую ошибку кроме main.Error, TypeError или RangeError ' +
            'запрос завершается с ошибкой -32000, "Unknown error"',
            function (done) {
                var peers = main.Peer.createPair();

                peers[0].request('testError').onFinish(function (error) {
                    delete main.Peer.Controller.testError;
                    assert.ok(error instanceof main.Error, 'Ошибка не получена');
                    assert.strictEqual(error.code, -32000, 'Неверный код ошибки');
                    assert.strictEqual(error.message, 'Unknown error', 'Неверное сообщение ошибки');
                    done();
                });
                main.Peer.Controller.testError = function () {
                    throw new Error('Sample Error');
                };
            });

        it('Если функция процедуры вернёт объект типа PromiseA, то ответ на запрос будет сформирован по результату ' +
            'разрешения promise',
            function (done) {
                var peers = main.Peer.createPair(),
                    procedure_res = 'result';

                peers[0].request('testPromise').onFinish(function (error, result) {
                    delete main.Peer.Controller.testPromise;
                    assert.strictEqual(result, procedure_res, 'Результат должен быть ' + procedure_res);
                    done();
                });

                main.Peer.Controller.testPromise = function () {
                    var promise = new main.Peer.Promise();
                    setTimeout(function () {
                        promise.resolve(procedure_res);
                    }, 10);
                    return promise;
                };
            });

        it('Если функция процедуры не вернёт объект типа PromiseA, то ответ на запрос будет сформирован с ' +
            'возвращённым значением',
            function (done) {
                var peers = main.Peer.createPair(),
                    procedure_res = 'result';

                peers[0].request('testRes').onFinish(function (error, result) {
                    delete main.Peer.Controller.testRes;
                    assert.strictEqual(result, procedure_res, 'Результат должен быть ' + procedure_res);
                    done();
                });

                main.Peer.Controller.testRes = function () {
                    return procedure_res;
                };
            });
    });

    describe('#_acceptResponse', function () {
        it('В списке запросов, ожидающих ответа, будет найден запрос с соответствующим id, для найденного запроса ' +
            'будет зафиксирован ответ (см. main.Request#finish)',
            function (done) {
                var peers = main.Peer.createPair(),
                    completed = 0,
                    called = 0,
                    procedure_result = 'result',
                    procedure_error = new main.Error(666, 'Sample Error'),
                    request_result = new main.Request('request_result', 'test'),
                    response_result = new main.Response(request_result.id, null, procedure_result),
                    request_error = new main.Request('request_error', 'test'),
                    response_error = new main.Response(request_error.id, procedure_error);

                request_result.finish = function () {
                    delete request_result.finish;
                    called++;
                    return main.Request.prototype.finish.apply(this, arguments);
                };

                request_error.finish = function () {
                    delete request_error.finish;
                    called++;
                    return main.Request.prototype.finish.apply(this, arguments);
                };

                request_result.onFinish(function (error, result) {
                    assert.strictEqual(result, procedure_result, 'Результат должен быть ' + procedure_result);
                    if (++completed === 2) {
                        assert.strictEqual(called, 2, 'Метод main.Request#finish не был вызван');
                        done();
                    }
                });

                request_error.onFinish(function (error) {
                    assert.ok(error instanceof main.Error, 'Процедура должна завершиться ошибкой');
                    assert.strictEqual(error.code, procedure_error.code, 'Код ошибки не совпадает с заданным');
                    assert.strictEqual(error.message, procedure_error.message,
                        'Сообщение ошибки не совпадает с заданным');
                    if (++completed === 2) {
                        assert.strictEqual(called, 2, 'Метод main.Request#finish не был вызван');
                        done();
                    }
                });

                peers[0]._outgoingRequests[request_result.id] = request_result;
                peers[0]._outgoingRequests[request_error.id] = request_error;

                peers[0]._acceptResponse(response_result);
                peers[0]._acceptResponse(response_error);
            });

        it('Если соответствующий запрос не найден, будет создано событие error',
            function () {
                var peers = main.Peer.createPair(),
                    handled = false;

                peers[0]._errorHandler = function (error) {
                    handled = true;
                    delete peers._errorHandler;
                    assert.ok(error instanceof Error, 'Сгенерированная ошибка не является экземпляром Error');
                };
                peers[0]._acceptResponse(new main.Response('some_request_id', null, true));
                assert.ok(handled, 'Событие error не было создано');
            });

        it('Если ответ не собержит id запроса, то будет создано событие error, ' +
            'парамтером события будет полученная ошибка',
            function () {
                var peers = main.Peer.createPair(),
                    handled = false,
                    error_jsonrpc = new main.Error(666, 'Sample Error');

                peers[0]._errorHandler = function (error) {
                    handled = error;
                    delete peers._errorHandler;
                    assert.equal(error, error_jsonrpc, 'Зарегисртирована какая-то левая ошибка');
                };
                peers[0]._acceptResponse(new main.Response(null, error_jsonrpc));
                assert.ok(handled, 'Ошибка не была зарегистрирована');
            });
    });

    describe('#_getRequestedProcedure', function () {
        it('Метод обастрактный и не имеет реализации',
            function () {
                assert.throws(function () {
                    (new main.Peer())._getRequestedProcedure();
                });
            });

        xit('Возвращает функцию, соответсвующую запрошенной процедуре');

        xit('Функция может быть представлена объектом вида {function: Function, context: Object}, где ' +
            'поле function сожержит функцию процедуры, а context - контекст вызова функции');
    });

    describe('#send', function () {
        it('Метод обастрактный и не имеет реализации',
            function () {
                assert.throws(function () {
                    (new main.Peer()).send();
                });
            });

        xit('Производит отправку сообщений из буфера Peer#_sendBuffer в порядке очереди');

        xit('После отправки сообщения, оно удаляется из очереди');
    });

    describe('#request', function () {
        it('Создает запрос, регистрирует его в списке исходящих запросов, производит попытку отправить запрос',
            function (done) {
                var peers = main.Peer.createPair(),
                    request = peers[0].request('ping', {payload: 'hello'});

                assert.notStrictEqual(request.id, null, 'У запроса пустой id');
                assert.ok(request instanceof main.Request, 'Запрос не был создан');
                assert.strictEqual(request, peers[0]._outgoingRequests[request.id],
                    'Запрос не зарегистрирован в списке исходящих');
                assert.doesNotThrow(function () {
                    request.onFinish(function (error, result) {
                        assert.strictEqual('hello', result);
                        done(error);
                    });
                });
            });

        it('После получения ответа на запрос, запрос удаляется из списка исходящих запросов ожидающих ответа',
            function (done) {
                var peers = main.Peer.createPair(),
                    request = peers[0].request('ping', {payload: 'hello'});

                assert.strictEqual(request, peers[0]._outgoingRequests[request.id]);
                request.onFinish(function (error, result) {
                    assert.strictEqual(request.id in peers[0]._outgoingRequests, false, 'Запрос всё ещё в списке');
                    done(error);
                });
            });

        it('При создании запроса устанавливает таймаут его выполнения, если ответ на запрос не получен до ' +
            'наступления таймаута, то запрос будет завершён с ошибкой "Request timeout" (код -32001)',
            function (done) {
                var peers = main.Peer.createPair({request_timeout: 0.05});

                peers[0].request('sleep', {ms: 1000}).onFinish(function (error, result) {
                    assert.ok(error instanceof main.Error, 'Запрос должен провалиться с ошибкой main.Error');
                    assert.strictEqual(error.code, -32001, 'Код ошибки должен быть -32001');
                    assert.strictEqual(error.code, -32001, 'Сообщение ошибки должно быть "Request timeout"');
                    done();
                });
            });
    });

    describe('#notify', function () {
        it('Создает уведомление, производит попытку отправить его',
            function (done) {
                var peers = main.Peer.createPair(),
                    request = peers[0].notify('ping', {payload: 'hello'});

                assert.strictEqual(request.id, null, 'У запроса не пустой id');
                assert.ok(request instanceof main.Request, 'Запрос не был создан');
                assert.doesNotThrow(function () {
                    request.onFinish(function (error, result) {
                        assert.strictEqual(null, result);
                        done(error);
                    });
                });
            });

        it('До тех пор, пока уведомление не будет отправлено оно содержится в списке исходящих Peer#_outgoingRequests',
            function (done) {
                var peers = main.Peer.createPair(),
                    request;

                // sync sending
                request = peers[0].notify('ping', {payload: 'hello'});
                assert.ok(!in_list(peers[0]._outgoingRequests, request), 'Запрос в списке после отправки');

                // async sending
                peers[0].send = function (message) {
                    setTimeout(function () {
                        peers[0].send(message);
                    }, 0);
                    delete peers[0].send;
                };
                request = peers[0].notify('ping', {payload: 'hello'});
                assert.ok(in_list(peers[0]._outgoingRequests, request), 'Не отправленный запрос не в списке');
                request.onFinish(function () {
                    assert.ok(!in_list(peers[0]._outgoingRequests, request), 'Запрос в списке после отправки');
                    done();
                });
            });

        it('При создании уведомления устанавливает таймаут его отправки, если его не удалось отправить до ' +
            'наступления таймаута, то запрос будет завершён с ошибкой "Request timeout" (код -32001)',
            function (done) {
                var peers = main.Peer.createPair({request_timeout: 0.05});

                // async sending
                peers[0].send = function (message) {
                    setTimeout(function () {
                        peers[0].send(message);
                    }, peers[0]._options.request_timeout * 1000 + 1);
                    delete peers[0].send;
                };
                peers[0].notify('ping', {payload: 'hello'}).onFinish(function (error, result) {
                    assert.ok(error instanceof main.Error, 'Уведомление должно провалиться с ошибкой main.Error');
                    assert.strictEqual(error.code, -32001, 'Код ошибки должен быть -32001');
                    assert.strictEqual(error.code, -32001, 'Сообщение ошибки должно быть "Request timeout"');
                    done();
                });
            });
    });
});