/**
 * @fileOverview Тесты класса [main.Error]{@link module:main.Error}
 * @ignore
 */

/* global describe, it */
var assert = require('assert'),
    main = require('../'),
    to_json = main.JSON.stringify;

describe('main.Error', function() {
    it('Должен быть унаследован от стандартного класса ошибок Error',
        function () {
            assert.strictEqual((new main.Error(0, '')) instanceof Error, true);
        });

    describe('#constructor', function () {
        var create = function () {
            var args = Array.prototype.slice.call(arguments);
            return function () {
                switch (args.length) {
                    case 0:
                        return new main.Error();

                    case 1:
                        return new main.Error(args[0]);

                    case 2:
                        return new main.Error(args[0], args[1]);

                    default:
                        return new main.Error(args[0], args[1], args[2]);
                }
            };
        };

        it('Первые два параметра конструктора обязательны',
            function () {
                assert.throws(create(), TypeError);
                assert.throws(create(1), TypeError);
                assert.throws(create(undefined, ''), TypeError);
                assert.doesNotThrow(create(10, 'Error message'));
            });

        it('Параметр code обязательный и должен быть целым числом',
            function () {
                assert.throws(create(undefined, 'Error message'), TypeError);
                assert.throws(create(null, 'Error message'), TypeError);
                assert.throws(create(0.1, 'Error message'), TypeError);
                assert.throws(create('10', 'Error message'), TypeError);
                assert.throws(create({}, 'Error message'), TypeError);
                assert.doesNotThrow(create(10, 'Error message'));
            });

        it('Параметр message обязательный и должен быть целым числом или строкой',
            function () {
                assert.throws(create(10, undefined), TypeError);
                assert.throws(create(10, null), TypeError);
                assert.throws(create(10, 100), TypeError);
                assert.throws(create(10, {}), TypeError);
                assert.doesNotThrow(create(0, ''));
            });

        it('Параметр data не обязательный, если представлен, то простым типом или структурой данных',
            function () {
                assert.throws(create(0, '', function () {}), TypeError);
                assert.doesNotThrow(create(0, '', new Error('Error message')), TypeError);
                assert.doesNotThrow(create(0, '', {}), TypeError);
                assert.doesNotThrow(create(0, '', []), TypeError);
                assert.doesNotThrow(create(0, '', ""), TypeError);
                assert.doesNotThrow(create(0, '', 0), TypeError);
                assert.doesNotThrow(create(0, '', true), TypeError);
            });
    });

    describe('#toJSON', function () {
        it('Возвращает представление RPC ошибки в виде простого объекта, ' +
            'который может быть использован для JSON сериализации, ' +
            'объект включает поля code, message и data (если представлено и не null)',
            function () {
                var code = 10,
                    message = 'Error message',
                    data = {field: 'value'},
                    e1 = new main.Error(code, message),
                    e2 = new main.Error(code, message, data),
                    json1 = e1.toJSON(),
                    json2 = e2.toJSON();

                assert.strictEqual(to_json(json1), to_json({code: code, message: message}));
                assert.strictEqual(to_json(json2), to_json({code: code, message: message, data: data}));
            });
    });

    describe('#toString', function () {
        it('Создают строку с JSON представлением объекта, включая поля code, message и data ' +
            '(если представлено и не null)',
            function () {
                var code = 10,
                    message = 'Error message',
                    data = {field: 'value'},
                    e1 = new main.Error(code, message),
                    e2 = new main.Error(code, message, data),
                    json1 = e1.toString(),
                    json2 = e2.toString();

                assert.strictEqual(json1, to_json({code: code, message: message}));
                assert.strictEqual(json2, to_json({code: code, message: message, data: data}));
            });
    });
});