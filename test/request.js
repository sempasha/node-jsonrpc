/**
 * @fileOverview Тесты класса [main.Request]{@link module:main.Request}
 * @ignore
 */

/* global describe, it */
var assert = require('assert'),
    main = require('../'),
    to_json = main.JSON.stringify;

describe('main.Request', function() {

    describe('#constructor', function () {
        var create = function (id, error, result) {
            return function () {
                return new main.Request(id, error, result);
            };
        };

        it('Параметр id должен быть строкой, целым числом, либо null (для уведомлений)',
            function () {
                assert.throws(create(undefined, 'someMethod'), TypeError);
                assert.throws(create(0.1, 'someMethod'), TypeError);
                assert.throws(create({}, 'someMethod'), TypeError);
                assert.doesNotThrow(create(10, 'someMethod'));
                assert.doesNotThrow(create('10', 'someMethod'));
            });

        it('Параметр method должен быть строкой',
            function () {
                assert.throws(create('10', null), TypeError);
                assert.throws(create('10', 10), TypeError);
                assert.throws(create('10', {}), TypeError);
                assert.doesNotThrow(create('10', 'someMethod'));
            });

        it('Параметр params должен быть либо опущен (если вызываемая процедура не принимает параметров), ' +
            'либо быть массивом (в случае перечисления параметров по порядку), ' +
            'либо бять простым объектом (в случае перечисления параметров по имени)',
            function () {
                assert.throws(create('10', 'someMethod', true), TypeError);
                assert.throws(create('10', 'someMethod', 10), TypeError);
                assert.throws(create('10', 'someMethod', '10'), TypeError);
                assert.throws(create('10', 'someMethod', new Date()), TypeError);
                assert.doesNotThrow(create('10', 'someMethod'));
                assert.doesNotThrow(create('10', 'someMethod', ['value']));
                assert.doesNotThrow(create('10', 'someMethod', {name: 'value'}));
            });
    });

    describe('#finish', function () {
        it('Параметр error должен быть экземпляром класса main.Error, либо null',
            function () {
                var finish = function (error) {
                    return function () {
                        (new main.Request('10', 'someMethod')).finish(error);
                    };
                };
                assert.throws(finish('Error message'), TypeError);
                assert.throws(finish(new Error('Error message')), TypeError);
                assert.doesNotThrow(finish(new main.Error(10, 'Error message')));
            });

        it('Параметр result может иметь какое угодно значение, кроме undefined',
            function () {
                var finish = function (result) {
                    return function () {
                        (new main.Request('10', 'someMethod')).finish(null, result);
                    };
                };
                assert.doesNotThrow(finish());
                assert.doesNotThrow(finish(null));
                assert.doesNotThrow(finish(10));
                assert.doesNotThrow(finish('done'));
                assert.doesNotThrow(finish({ten: 10}));
            });

        it('Параметр error и result не могут быть определены одновременно, ' +
            'так как вызов процедуры может завершиться либо ошибкой, либюо успехом',
            function () {
                var finish = function (error, result) {
                    return function () {
                        (new main.Request('10', 'someMethod')).finish(error, result);
                    };
                };

                assert.throws(finish(new main.Error(10, 'Error message'), true), TypeError);
            });

        it('Вызов процедуры может быть завершён единажды',
            function () {
                var finish = function (request, error, result) {
                        return function () {
                            request.finish(error, result);
                        };
                    },
                    request = new main.Request('10', 'someMethod'),
                    error = new main.Error(10, 'Error message'),
                    result = true;

                assert.doesNotThrow(finish(request, error));
                assert.throws(finish(request, null, result), Error, 'Can\'t finish request twice');
            });

        it('Когда становится известен результат выполнения запроса, ' +
            'должны быть вызываны обработчики добавленные через #onFinish',
            function () {
                var called = false,
                    request = new main.Request('10', 'someMethod'),
                    handler = function () {
                        called = true;
                    };

                request.onFinish(handler);
                request.finish(null, 'result');
                assert.ok(called, 'Обработчик не был вызван');
            });
    });

    describe('#onFinish', function () {
        it('Принимает только функцию, в качестве параметра',
            function () {
                var request = new main.Request('10', 'someMethod'),
                    add_handler = function (handler) {
                        return function () {
                            request.onFinish(handler);
                        };
                    };

                assert.throws(add_handler(), TypeError);
                assert.throws(add_handler(null), TypeError);
                assert.throws(add_handler(10), TypeError);
                assert.throws(add_handler('function'), TypeError);
                assert.throws(add_handler({}), TypeError);
                assert.doesNotThrow(add_handler(function () {}), TypeError);
            });

        it('При завершении запроса обработчики, добавленные через метод #onFinish будут вызваны порядке их добавления',
            function (done) {
                var request = new main.Request('10', 'someMethod'),
                    handlers_count = 0,
                    handlers_called_count = 0,
                    add_handler = function () {
                        var handlers_order = ++handlers_count;
                        request.onFinish(function () {
                            handlers_called_count++;
                            assert.strictEqual(handlers_called_count, handlers_order, 'Вызов не в порядке добавления');
                            if (handlers_called_count === handlers_count) {
                                done();
                            }
                        });
                    },
                    count = 10;

                while(count-- > 0) {
                    add_handler();
                }
                request.finish(null, 'result');
            });

        it('При добавлении обработчиков после завершения запроса, они будут вызваны сразу после добавления',
            function () {
                var called = false,
                    request = new main.Request('10', 'someMethod'),
                    handler = function () {
                        called = true;
                    };

                request.finish(null, 'result');
                request.onFinish(handler);
                assert.ok(called, 'Обработчик не был вызван');
            });

        it('При вызове обработчика в него передаются аргументы error - ошибка вызова процедуры и ' +
            'result - результат выполнения процедуры, значения параметров соответствуют тем, ' +
            'что были переданы в метод finish при определении результата выполнения запроса',
            function (done) {
                var request1 = new main.Request('10', 'someMethod'),
                    request2 = new main.Request('11', 'someMethod'),
                    error = new main.Error(10, 'Error message'),
                    result = 'result',
                    handler1 = function (e, r) {
                        assert.strictEqual(e, error, 'Ошибка не соответствует заданной методом #finish');
                        assert.strictEqual(r, null, 'Результат должен быть null');
                    },
                    handler2 = function (e, r) {
                        assert.strictEqual(e, null, 'Ошибка должны быть null');
                        assert.strictEqual(r, result, 'Результат не соответствует заданному методом #finish');
                    };

                request1.onFinish(handler1);
                request2.onFinish(handler2);
                request1.finish(error);
                request2.finish(null, result);
                setTimeout(done, 0);
            });
    });

    describe('#toJSON', function () {
        it('Возвращает представление RPC запроса в виде простого объекта, ' +
            'который может быть использован для JSON сериализации, ' +
            'объект включает поля jsonrpc="2.0", method и params (если параметры заданы)',
            function () {
                var id = '10',
                    method = 'someMethod',
                    params = {name: 'value'},
                    r1 = new main.Request(null, method),
                    r2 = new main.Request(null, method, params),
                    r3 = new main.Request(id, method),
                    r4 = new main.Request(id, method, params),
                    json1 = r1.toJSON(),
                    json2 = r2.toJSON(),
                    json3 = r3.toJSON(),
                    json4 = r4.toJSON();

                assert.strictEqual(to_json(json1), to_json({jsonrpc: '2.0', method: method}));
                assert.strictEqual(to_json(json2), to_json({jsonrpc: '2.0', method: method, params: params}));
                assert.strictEqual(to_json(json3), to_json({jsonrpc: '2.0', id: id, method: method}));
                assert.strictEqual(to_json(json4), to_json({jsonrpc: '2.0', id: id, method: method, params: params}));
            });
    });

    describe('#toString', function () {
        it('Создают строку с JSON представлением объекта, ' +
            'включая поля jsonrpc="2.0", method и params (если параметры заданы)',
            function () {
                var id = '10',
                    method = 'someMethod',
                    params = {name: 'value'},
                    r1 = new main.Request(null, method),
                    r2 = new main.Request(null, method, params),
                    r3 = new main.Request(id, method),
                    r4 = new main.Request(id, method, params),
                    json1 = r1.toString(),
                    json2 = r2.toString(),
                    json3 = r3.toString(),
                    json4 = r4.toString();

                assert.strictEqual(json1, to_json({jsonrpc: '2.0', method: method}));
                assert.strictEqual(json2, to_json({jsonrpc: '2.0', method: method, params: params}));
                assert.strictEqual(json3, to_json({jsonrpc: '2.0', id: id, method: method}));
                assert.strictEqual(json4, to_json({jsonrpc: '2.0', id: id, method: method, params: params}));
            });
    });
});