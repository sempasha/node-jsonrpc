/**
 * @fileOverview Тесты класса [main.Response]{@link module:main.Response}
 * @ignore
 */

/* global describe, it */
var assert = require('assert'),
    main = require('../'),
    to_json = main.JSON.stringify;

describe('main.Response', function() {
    it('Класс предназначен для представления ответного сообщения по протоколу JSON-RPC 2.0', function () {

    });

    describe('#constructor', function () {
        var create = function (id, error, result) {
            return function () {
                return new main.Response(id, error, result);
            };
        };

        it('Параметр id должен быть строкой, целым числом, либо null (если id запроса не определён)',
            function () {
                assert.throws(create(undefined, null, true), TypeError);
                assert.throws(create(0.1, null, true), TypeError);
                assert.throws(create({}, null, true), TypeError);
                assert.doesNotThrow(create(10, null, true));
                assert.doesNotThrow(create('10', null, true));
            });

        it('Ситуация, когда id не определён (id=null) возможна только при возникновении ошибки процедуры',
            function () {
                assert.throws(create(null, null, true), TypeError);
                assert.doesNotThrow(create(null, new main.Error(10, 'Error message')));
            });

        it('Параметр error должен быть либо ошибкой типа main.Error, либо null',
            function () {
                assert.throws(create('10', 10), TypeError);
                assert.throws(create('10', 'error'), TypeError);
                assert.throws(create('10', new Error('Error text')), TypeError);
                assert.doesNotThrow(create('10', null, null));
                assert.doesNotThrow(create('10', new main.Error(10, 'Error message')));
            });

        it('Параметр result должен содержать результат выполнения процедуры, результат может быть любым ' +
            '(в том числе null, но не undefined)',
            function () {
                assert.throws(create('10', null));
                assert.throws(create('10', null, undefined));
                assert.doesNotThrow(create('10', null, null));
                assert.doesNotThrow(create('10', null, 10));
                assert.doesNotThrow(create('10', null, '10'));
                assert.doesNotThrow(create('10', null, {}));
            });

        it('Ситуация, когда и error=null и result=null считается нормальной, так как это означает, ' +
            'что ошибки не произошло, а результат выполнения операции равен null',
            function () {
                assert.doesNotThrow(create(0, null, null));
            });

        it('В ответе одновременно должны быть определены либо error, либо result, но не оба сразу',
            function () {
                assert.throws(create(10, new main.Error(10, 'Error message'), true));
                assert.throws(create(10, new main.Error(10, 'Error message'), false));
            });
    });

    describe('#toJSON', function () {
        it('Возвращает представление RPC ответа в виде простого объекта, ' +
            'который может быть использован для JSON сериализации, ' +
            'объект включает поля jsonrpc="2.0", error либо result' +
            '(в зависимости от результата выполнения процедуры)',
            function () {
                var id = '10',
                    error = new main.Error(10, 'Error message'),
                    result = {field: 'value'},
                    r1 = new main.Response(null, error),
                    r2 = new main.Response(id, error),
                    r3 = new main.Response(id, null, result),
                    json1 = r1.toJSON(),
                    json2 = r2.toJSON(),
                    json3 = r3.toJSON();

                assert.strictEqual(to_json(json1), to_json({jsonrpc: '2.0', id: null,
                    error: {code: 10, message: 'Error message'}}));
                assert.strictEqual(to_json(json2), to_json({jsonrpc: '2.0', id: id,
                    error: {code: 10, message: 'Error message'}}));
                assert.strictEqual(to_json(json3), to_json({jsonrpc: '2.0', id: id, result: result}));
            });
    });

    describe('#toString', function () {
        it('Создают строку с JSON представлением объекта, включая поля jsonrpc="2.0", error либо result ' +
            '(в зависимости от результата выполнения процедуры)',
            function () {
                var id = '10',
                    error = new main.Error(10, 'Error message'),
                    result = {field: 'value'},
                    r1 = new main.Response(null, error),
                    r2 = new main.Response(id, error),
                    r3 = new main.Response(id, null, result),
                    json1 = r1.toString(),
                    json2 = r2.toString(),
                    json3 = r3.toString();

                assert.strictEqual(json1, to_json({jsonrpc: '2.0', id: null,
                    error: {code: 10, message: 'Error message'}}));
                assert.strictEqual(json2, to_json({jsonrpc: '2.0', id: id,
                    error: {code: 10, message: 'Error message'}}));
                assert.strictEqual(json3, to_json({jsonrpc: '2.0', id: id, result: result}));
            });
    });
});