/**
 * @fileOverview Тесты функции [main.isPromise]{@link module:main.isPromise}
 * @ignore
 */

/* global describe, it */
var assert = require('assert'),
    main = require('../');

describe('main.isPromise', function() {
    it('Проверяет является ли сущность объектом типа Promise/A',
        function () {
            var test;

            test = 1;
            assert.strictEqual(main.isPromise(test), false);

            test = 'Promise';
            assert.strictEqual(main.isPromise(test), false);

            test = null;
            assert.strictEqual(main.isPromise(test), false);

            test = function () {};
            assert.strictEqual(main.isPromise(test), false);

            test = {promise: {then: function () {}}, done: function () {}, fail: function () {}};
            assert.strictEqual(main.isPromise(test), false);

            test = {then: function () {}};
            assert.strictEqual(main.isPromise(test), true);
        });
});