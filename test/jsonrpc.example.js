/**
 * @fileOverview Реализация расширения для класса [main.Peer]{@link module:main.Peer}, для тестирования этого же класса
 * @ignore
 */

/**
 * @ignore
 */
var inherits = require('util').inherits;

/**
 * @ignore
 */
var EventEmitter = require('events').EventEmitter;

/**
 * @ignore
 */
var SuperPeer = require('../lib/peer');

/**
 * @ignore
 */
var noop = function () {};

/**
 * @ignore
 */
module.exports = exports = require('../');

/**
 * @ignore
 */
exports.logger = {log: noop, debug: noop, info: noop, warn: noop, error: noop};

/**
 * Простейшая PromiseA имплементация, естественно не полностью соответствующая протоколу,
 * но тем не менее для тестов пойдёт
 *
 * @constructor
 */
var Promise = function () {
    var result,
        resolved = 0,
        success = [],
        fail = [];

    this.resolve = function (r) {
        if (resolved === 0) {
            resolved = 1;
            result = r;
            fail = [];
            while(success.length > 0) {
                success.shift().call(null, result);
            }
        }
    };
    this.reject = function (e) {
        if (resolved === 0) {
            resolved = -1;
            result = e;
            success = [];
            while(fail.length > 0) {
                fail.shift().call(null, result);
            }
        }
    };
    this.then = function (s, f) {
        switch (resolved) {
            case 0:
                if (s) {
                    success.push(s);
                }
                if (f) {
                    fail.push(f);
                }
                break;

            case 1:
                if (s) {
                    s(result);
                }
                break;

            case -1:
                if (f) {
                    f(result);
                }
                break;
        }
        return this;
    };
};

/**
 * Простейший контроллер
 *
 * @type {{ping: Function, sleep: Function}}
 */
var Controller = {
    ping: function (payload) {
        return payload;
    },

    sleep: function (ms) {
        if (typeof ms !== 'number' || ms % 1 !== 0) {
            throw new TypeError('Param ms must be an positive integer');
        } else if (ms < 0) {
            throw new RangeError('Param ms must be an positive integer');
        }
        var promise = new Promise();
        setTimeout(function () {
            promise.resolve();
        }, ms);
        return promise;
    }
};

/**
 * Эмуляция соединения с интерфейсом для отправки данных с одной стороны и получения с другой стороны,
 * для того, чтобы писать в соединение используйте Channel#in.write для того, чтобы
 *
 * @constructor
 */
var Channel = function () {
    var timer = null,
        buffer = [],
        write = function (message) {
            buffer.push(message);
            if (timer === null) {
                timer = setTimeout(transmit, Math.floor(10 * Math.random()));
            }
        },
        transmit = function () {
            output.emit('message', [buffer.shift()]);
            if (buffer.length > 0) {
                timer = setTimeout(transmit, Math.floor(10 * Math.random()));
            } else {
                timer = null;
            }
        },
        input = new EventEmitter(),
        output = new EventEmitter();

    input.write = write;
    this.in = input;
    this.out = output;
};

/**
 * Имплементация main.Peer, переобъявлены абстрактные методы main.Peer#_getRequestedProcedure
 * и main.Peer#send, а так же конструктор
 *
 * @param options
 * @param socket
 * @constructor
 */
var Peer = function (options, socket) {
    var self = this;
    SuperPeer.call(self, options);
    self._socket = socket;
    socket.on('message', function (message) {
        self._acceptMessage(message);
    });
};

/**
 * Экспозиция Promise
 *
 * @type {Promise}
 */
Peer.Promise = Promise;

/**
 * Экспозиция контроллера
 *
 * @type {{ping: Function, sleep: Function}}
 */
Peer.Controller = Controller;

/**
 * Создаём пару пиров для обмена JSON-RPC
 *
 * @returns {Peer[]}
 */
Peer.createPair = function (options1, options2) {
    var channel1 = new Channel(),
        channel2 = new Channel(),
        socket1 = {
            write: function (data) {
                return channel1.in.write(data);
            },
            on: function (type, listener) {
                return channel2.out.on(type, listener);
            }
        },
        socket2 = {
            write: function (data) {
                return channel2.in.write(data);
            },
            on: function (type, listener) {
                return channel1.out.on(type, listener);
            }
        },
        peer1 = new Peer(options1, socket1),
        peer2 = new Peer(options2, socket2);

    return [peer1, peer2];
};

inherits(Peer, SuperPeer);
// экспонируем расширенный класс
exports.Peer = Peer;

/**
 * Переобъявляем метод [main.Peer#_getRequestedProcedure]{@link module:main.Peer#_getRequestedProcedure}
 *
 * @returns {{function: Function, context: null}}
 */
Peer.prototype._getRequestedProcedure = function (method) {
    if (typeof Controller[method] !== 'undefined') {
        return {
            'function': Controller[method],
            context: null
        };
    } else {
        return null;
    }
};

/**
 * Переобъявляем метод [main.Peer#send]{@link module:main.Peer#send}
 *
 * @returns {Undefined}
 */
Peer.prototype.send = function () {
    while(this._sendBuffer.length > 0) {
        var message = this._sendBuffer.shift();
        if (message instanceof exports.Request) {
            if (!message.isFinished()) {
                this._socket.write(message.toString());
                if (message.id === null) {
                    message.finish();
                }
            }
        } else {
            this._socket.write(message.toString());
        }
    }
};