/**
 * @fileOverview Тесты функций [main.createArgsList]{@link module:main.createArgsList} и
 * [main.setArgsMap]{@link module:main.setArgsMap}
 * @ignore
 */

/* global describe, it */
var assert = require('assert'),
    main = require('../');

describe('main.setArgsMap', function() {
    it('Задаёт список параметров для функции-процедуры',
        function () {
            var arguments_map = ['param1', 'param2'],
                procedure = function () {

                },
                params = {param1: 'value1', param2: 'value2'},
                params_list;

            main.setArgsMap(procedure, arguments_map);
            params_list = main.createArgsList(procedure, params);
            assert.strictEqual(params_list[0], params.param1);
            assert.strictEqual(params_list[1], params.param2);
        });
});

describe('main.createArgsList', function() {
    it('Если список аргументов задан по порядку (в виде массива) - ' +
        'i-ый элемент массива содержит значение i-ого аргумента функции',
        function () {
            var params = ['value1', 'value2'],
                method = function (param1, param2) {},
                args = main.createArgsList(method, params);

            params.forEach(function (value, index) {
                assert.strictEqual(value, args[index]);
            });
        });

    it('Если список аргументов задан по имени (в виде хэш-мап объекта) - ' +
        'значение элемента списка под именем  соответствует аргументу функции с таким же именем',
        function () {
            var params = {param2: 'value2', param1: 'value1'},
                method = function (param1, param2) {},
                args = main.createArgsList(method, params);

            assert.strictEqual(args[0], params.param1);
            assert.strictEqual(args[1], params.param2);
        });

    it('Длина возвращаемого списка всегда совпадает с количеством аргументов функции',
        function () {
            var method = function (param1, param2) {},
                args_length = 2,
                params, args;

            params = ['value1'];
            args = main.createArgsList(method, params);
            assert.strictEqual(args.length, args_length);

            params = ['value1', 'value2', 'value3'];
            args = main.createArgsList(method, params);
            assert.strictEqual(args.length, args_length);

            params = {param1: 'value1'};
            args = main.createArgsList(method, params);
            assert.strictEqual(args.length, args_length);

            params = {param1: 'value1', param2: 'value2', param3: 'value3'};
            args = main.createArgsList(method, params);
            assert.strictEqual(args.length, args_length);
        });

    it('Если в исходном списке параметров отсутствуют значения каких-либо аргументов функции, то в возвращаемом ' +
        'списке значение этих параметров будет равно undefined',
        function () {
            var method = function (param1, param2) {},
                params, args;

            params = ['value1'];
            args = main.createArgsList(method, params);
            assert.strictEqual(typeof args[1], 'undefined');

            params = {param1: 'value1'};
            args = main.createArgsList(method, params);
            assert.strictEqual(typeof args[1], 'undefined');

            params = {param2: 'value2'};
            args = main.createArgsList(method, params);
            assert.strictEqual(typeof args[0], 'undefined');
        });
});