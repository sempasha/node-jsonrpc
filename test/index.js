/**
 * @fileOverview Пакет тестов для модуля
 * @ignore
 */

require('./is-promise');
require('./parse-message');
require('./procedure');
require('./error');
require('./request');
require('./response');
require('./peer');