/**
 * @fileOverview Тесты функции [main.parseMessage]{@link module:main.parseMessage}
 * @ignore
 */

/* global describe, it */
var assert = require('assert'),
    main = require('../');

describe('main.parseMessage', function() {
    it('При ошибке парсера JSON выбрасывает main.Error (-32700, Parse error)',
        function () {
            var error_code = -32700,
                error_message = 'Parse error',
                message = '{"jsonrpc": "2.0", "method": "someMethod", params: ["]}';

            assert.throws(function () {main.parseMessage(message);}, main.Error);
            try {
                main.parseMessage(message);
            } catch (error) {
                assert.ok(error instanceof main.Error, 'Ошибка должна иметь тип main.Error');
                assert.equal(error.code, error_code);
                assert.equal(error.message, error_message);
            }
        });

    it('При неверном формате сообщения выбрасывает main.Error (-32600, Invalid Request)',
        function () {
            var error_code = -32600,
                error_message = 'Invalid Request',
                invalid = [
                    ['"someMethod"', 'Сообщение должно быть объектом'],
                    ['{"method":"someMethod","params":["value"]}', 'Сообщение должно иметь поле jsonrpc'],
                    ['{"jsonrpc":"","method":"someMethod","params":["value"]}', 'Версия jsonrpc должна быть "2.0"'],
                    ['{"jsonrpc":"2.0"}', 'Для сообщения обязательно наличие поля id (null, строка либо int)'],
                    ['{"jsonrpc":"2.0","id":{}}', 'Для сообщения обязательно наличие поля id (null, строка либо int)'],
                    ['{"jsonrpc":"2.0","id":"10"}', 'Сообщение должно быть запросом (method), либо ответом ' +
                        '(error, result)'],
                    ['{"jsonrpc":"2.0","id":"10","method":10}', 'Поле method запроса должно быть строкой'],
                    ['{"jsonrpc":"2.0","id":null,"method":"someMethod","params":"value"}', 'Для запроса поле params ' +
                        'может быть пропущено или быть объектом или массивом'],
                    ['{"jsonrpc":"2.0","id":"10","error":{"code":10,"message":"Error message"},"result":true}',
                        'Ответ не может содержать поля error и result одновременно'],
                    ['{"jsonrpc":"2.0","id":"10","error":"Fuck"}', 'Поле error запроса должно быть объектом формата ' +
                        '{code: Number(Integer), message: String[, data: (Boolean|Number|String|Array|Object)]}'],
                    ['{"jsonrpc":"2.0","id":"10","error":{"code":"10","message":"Error message"}}',
                        'Поле error запроса должно быть объектом формата ' +
                        '{code: Number(Integer), message: String[, data: (Boolean|Number|String|Array|Object)]}'],
                    ['{"jsonrpc":"2.0","id":"10","error":{"code":10,"message":10101}}',
                        'Поле error запроса должно быть объектом формата ' +
                        '{code: Number(Integer), message: String[, data: (Boolean|Number|String|Array|Object)]}']
                ];

            invalid.forEach(function (item) {
                assert.throws(function () {main.parseMessage(item[0]);}, main.Error, item[1]);
                try {
                    main.parseMessage(item[0]);
                } catch (error) {
                    assert.ok(error instanceof main.Error, 'Отсутствует ошибка main.Error. ' + item[1]);
                    assert.equal(error.code, error_code, 'Невенрый код ошибки main.Error. ' + item[1]);
                    assert.equal(error.message, error_message, 'Невенрое сообщение ошибки main.Error. ' + item[1]);
                }
            });
        });

    it('При наличии в сообщении поля method возвращает main.Request',
        function () {
            var messages = [
                '{"jsonrpc":"2.0","id":1,"method":"someMethod","params":{"name":"value"}}'
            ];

            messages.forEach(function (message) {
                var request = main.parseMessage(message);
                assert.ok(request instanceof main.Request);
            });
        });

    it('При наличии в сообщении поля result или error возвращает main.Response',
        function () {
            var messages = [
                '{"jsonrpc":"2.0","id":10,"result":true}',
                '{"jsonrpc":"2.0","id":10,"error":{"code":10,"message":"Error message"}}'
            ];

            messages.forEach(function (message) {
                var response = main.parseMessage(message);
                assert.ok(response instanceof main.Response);
            });
        });
});